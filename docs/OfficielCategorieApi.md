# swagger_client.OfficielCategorieApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_officiel_categorie_collection**](OfficielCategorieApi.md#get_officiel_categorie_collection) | **GET** /api/officiel_categories | Retrieves the collection of OfficielCategorie resources.
[**get_officiel_categorie_item**](OfficielCategorieApi.md#get_officiel_categorie_item) | **GET** /api/officiel_categories/{id} | Retrieves a OfficielCategorie resource.

# **get_officiel_categorie_collection**
> InlineResponse20018 get_officiel_categorie_collection(page=page, sa_no=sa_no, sa_no=sa_no, ca_cod=ca_cod, ca_cod=ca_cod, ca_niv=ca_niv, ca_niv=ca_niv)

Retrieves the collection of OfficielCategorie resources.

Retrieves the collection of OfficielCategorie resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.OfficielCategorieApi()
page = 1  # int | The collection page number (optional) (default to 1)
sa_no = 56  # int |  (optional)
sa_no = [56]  # list[int] |  (optional)
ca_cod = 'ca_cod_example'  # str |  (optional)
ca_cod = ['ca_cod_example']  # list[str] |  (optional)
ca_niv = 'ca_niv_example'  # str |  (optional)
ca_niv = ['ca_niv_example']  # list[str] |  (optional)

try:
    # Retrieves the collection of OfficielCategorie resources.
    api_response = api_instance.get_officiel_categorie_collection(page=page, sa_no=sa_no, sa_no=sa_no, ca_cod=ca_cod,
                                                                  ca_cod=ca_cod, ca_niv=ca_niv, ca_niv=ca_niv)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OfficielCategorieApi->get_officiel_categorie_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **sa_no** | **int**|  | [optional] 
 **sa_no** | [**list[int]**](int.md)|  | [optional] 
 **ca_cod** | **str**|  | [optional] 
 **ca_cod** | [**list[str]**](str.md)|  | [optional] 
 **ca_niv** | **str**|  | [optional] 
 **ca_niv** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**InlineResponse20018**](InlineResponse20018.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_officiel_categorie_item**
> OfficielCategorieJsonldOfficielCategorie get_officiel_categorie_item(id)

Retrieves a OfficielCategorie resource.

Retrieves a OfficielCategorie resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.OfficielCategorieApi()
id = 'id_example'  # str | Resource identifier

try:
    # Retrieves a OfficielCategorie resource.
    api_response = api_instance.get_officiel_categorie_item(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OfficielCategorieApi->get_officiel_categorie_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Resource identifier | 

### Return type

[**OfficielCategorieJsonldOfficielCategorie**](OfficielCategorieJsonldOfficielCategorie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

