# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PouleJourneeJsonldPouleJournee(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'type': 'str',
        'context': 'OneOfPouleJourneeJsonldPouleJourneeContext',
        'number': 'int',
        '_date': 'datetime',
        'pj_lib_tour': 'str',
        'poule': 'PouleJsonldPouleJournee',
        'phase': 'PhaseJsonldPouleJournee',
        'competition': 'CompetJsonldPouleJournee',
        'name': 'str'
    }

    attribute_map = {
        'id': '@id',
        'type': '@type',
        'context': '@context',
        'number': 'number',
        '_date': 'date',
        'pj_lib_tour': 'pj_lib_tour',
        'poule': 'poule',
        'phase': 'phase',
        'competition': 'competition',
        'name': 'name'
    }

    def __init__(self, id=None, type=None, context=None, number=None, _date=None, pj_lib_tour=None, poule=None, phase=None, competition=None, name=None):  # noqa: E501
        """PouleJourneeJsonldPouleJournee - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._context = None
        self._number = None
        self.__date = None
        self._pj_lib_tour = None
        self._poule = None
        self._phase = None
        self._competition = None
        self._name = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if context is not None:
            self.context = context
        if number is not None:
            self.number = number
        if _date is not None:
            self._date = _date
        if pj_lib_tour is not None:
            self.pj_lib_tour = pj_lib_tour
        if poule is not None:
            self.poule = poule
        if phase is not None:
            self.phase = phase
        if competition is not None:
            self.competition = competition
        if name is not None:
            self.name = name

    @property
    def id(self):
        """Gets the id of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The id of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this PouleJourneeJsonldPouleJournee.


        :param id: The id of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The type of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this PouleJourneeJsonldPouleJournee.


        :param type: The type of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def context(self):
        """Gets the context of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The context of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: OneOfPouleJourneeJsonldPouleJourneeContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this PouleJourneeJsonldPouleJournee.


        :param context: The context of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: OneOfPouleJourneeJsonldPouleJourneeContext
        """

        self._context = context

    @property
    def number(self):
        """Gets the number of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The number of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: int
        """
        return self._number

    @number.setter
    def number(self, number):
        """Sets the number of this PouleJourneeJsonldPouleJournee.


        :param number: The number of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: int
        """

        self._number = number

    @property
    def _date(self):
        """Gets the _date of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The _date of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: datetime
        """
        return self.__date

    @_date.setter
    def _date(self, _date):
        """Sets the _date of this PouleJourneeJsonldPouleJournee.


        :param _date: The _date of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: datetime
        """

        self.__date = _date

    @property
    def pj_lib_tour(self):
        """Gets the pj_lib_tour of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The pj_lib_tour of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: str
        """
        return self._pj_lib_tour

    @pj_lib_tour.setter
    def pj_lib_tour(self, pj_lib_tour):
        """Sets the pj_lib_tour of this PouleJourneeJsonldPouleJournee.


        :param pj_lib_tour: The pj_lib_tour of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: str
        """

        self._pj_lib_tour = pj_lib_tour

    @property
    def poule(self):
        """Gets the poule of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The poule of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: PouleJsonldPouleJournee
        """
        return self._poule

    @poule.setter
    def poule(self, poule):
        """Sets the poule of this PouleJourneeJsonldPouleJournee.


        :param poule: The poule of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: PouleJsonldPouleJournee
        """

        self._poule = poule

    @property
    def phase(self):
        """Gets the phase of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The phase of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: PhaseJsonldPouleJournee
        """
        return self._phase

    @phase.setter
    def phase(self, phase):
        """Sets the phase of this PouleJourneeJsonldPouleJournee.


        :param phase: The phase of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: PhaseJsonldPouleJournee
        """

        self._phase = phase

    @property
    def competition(self):
        """Gets the competition of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The competition of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: CompetJsonldPouleJournee
        """
        return self._competition

    @competition.setter
    def competition(self, competition):
        """Sets the competition of this PouleJourneeJsonldPouleJournee.


        :param competition: The competition of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: CompetJsonldPouleJournee
        """

        self._competition = competition

    @property
    def name(self):
        """Gets the name of this PouleJourneeJsonldPouleJournee.  # noqa: E501


        :return: The name of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this PouleJourneeJsonldPouleJournee.


        :param name: The name of this PouleJourneeJsonldPouleJournee.  # noqa: E501
        :type: str
        """

        self._name = name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PouleJourneeJsonldPouleJournee, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PouleJourneeJsonldPouleJournee):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
