# CompetJsonldCompet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfCompetJsonldCompetContext** |  | [optional] 
**cp_no** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**license_code** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**shortname** | **str** |  | [optional] 
**cp_niv** | **str** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgJsonldCompet**](CdgJsonldCompet.md) |  | [optional] 
**phases** | [**list[PhaseJsonldCompet]**](PhaseJsonldCompet.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

