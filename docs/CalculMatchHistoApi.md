# swagger_client.CalculMatchHistoApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_calcul_match_histo_collection**](CalculMatchHistoApi.md#get_calcul_match_histo_collection) | **GET** /api/calcul_match_histos | Retrieves the collection of CalculMatchHisto resources.
[**get_calcul_match_histo_item**](CalculMatchHistoApi.md#get_calcul_match_histo_item) | **GET** /api/calcul_match_histos/{id} | Retrieves a CalculMatchHisto resource.

# **get_calcul_match_histo_collection**
> InlineResponse2003 get_calcul_match_histo_collection(page=page, cl_no1=cl_no1, cl_no2=cl_no2, lc_cod1=lc_cod1, eq_cod1=eq_cod1, lc_cod2=lc_cod2, eq_cod2=eq_cod2)

Retrieves the collection of CalculMatchHisto resources.

Retrieves the collection of CalculMatchHisto resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculMatchHistoApi()
page = 1  # int | The collection page number (optional) (default to 1)
cl_no1 = [56]  # list[int] |  (optional)
cl_no2 = [56]  # list[int] |  (optional)
lc_cod1 = ['lc_cod1_example']  # list[str] |  (optional)
eq_cod1 = [56]  # list[int] |  (optional)
lc_cod2 = ['lc_cod2_example']  # list[str] |  (optional)
eq_cod2 = [56]  # list[int] |  (optional)

try:
    # Retrieves the collection of CalculMatchHisto resources.
    api_response = api_instance.get_calcul_match_histo_collection(page=page, cl_no1=cl_no1, cl_no2=cl_no2,
                                                                  lc_cod1=lc_cod1, eq_cod1=eq_cod1, lc_cod2=lc_cod2,
                                                                  eq_cod2=eq_cod2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculMatchHistoApi->get_calcul_match_histo_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_no1** | [**list[int]**](int.md)|  | [optional] 
 **cl_no2** | [**list[int]**](int.md)|  | [optional] 
 **lc_cod1** | [**list[str]**](str.md)|  | [optional] 
 **eq_cod1** | [**list[int]**](int.md)|  | [optional] 
 **lc_cod2** | [**list[str]**](str.md)|  | [optional] 
 **eq_cod2** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calcul_match_histo_item**
> CalculMatchHistoJsonldCalculMatchHisto get_calcul_match_histo_item(id)

Retrieves a CalculMatchHisto resource.

Retrieves a CalculMatchHisto resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculMatchHistoApi()
id = 'id_example'  # str | Resource identifier

try:
    # Retrieves a CalculMatchHisto resource.
    api_response = api_instance.get_calcul_match_histo_item(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculMatchHistoApi->get_calcul_match_histo_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Resource identifier | 

### Return type

[**CalculMatchHistoJsonldCalculMatchHisto**](CalculMatchHistoJsonldCalculMatchHisto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

