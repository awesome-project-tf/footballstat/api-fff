# PouleMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**cdg** | [**CdgMatchMembre**](CdgMatchMembre.md) |  | [optional] 
**poule_unique** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

