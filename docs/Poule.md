# Poule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**code** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**gp_diff_info** | **str** |  | [optional] 
**gp_diff_no_tour** | **int** |  | [optional] 
**phase** | **str** |  | [optional] 
**competition** | **str** |  | [optional] 
**cdg** | **str** |  | [optional] 
**rounds** | **list[str]** |  | [optional] 
**classement_journees** | **list[str]** |  | [optional] 
**calcul_classement** | [**list[CalculClassement]**](CalculClassement.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**id** | **int** |  | [optional] 
**external_message_id** | **str** |  | [optional] 
**calcul_classements** | [**list[CalculClassement]**](CalculClassement.md) |  | [optional] 
**poule_unique** | **bool** |  | [optional] 
**diffusable** | **bool** |  | [optional] 
**matchs** | **bool** |  | [optional] 
**at_least_one_match_resultat** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

