# EngagementMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competition** | [**CompetMatch**](CompetMatch.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

