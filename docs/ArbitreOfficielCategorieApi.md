# swagger_client.ArbitreOfficielCategorieApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_arbitre_officiel_categorie_collection**](ArbitreOfficielCategorieApi.md#get_arbitre_officiel_categorie_collection) | **GET** /api/arbitre_officiel_categories | Retrieves the collection of ArbitreOfficielCategorie resources.
[**get_arbitre_officiel_categorie_item**](ArbitreOfficielCategorieApi.md#get_arbitre_officiel_categorie_item) | **GET** /api/arbitre_officiel_categories/{id} | Retrieves a ArbitreOfficielCategorie resource.

# **get_arbitre_officiel_categorie_collection**
> InlineResponse200 get_arbitre_officiel_categorie_collection(page=page, officiel_categorie_id=officiel_categorie_id, officiel_categorie_id=officiel_categorie_id, cg_no=cg_no, cg_no=cg_no, officiel_categorie_sa_no=officiel_categorie_sa_no, officiel_categorie_sa_no=officiel_categorie_sa_no)

Retrieves the collection of ArbitreOfficielCategorie resources.

Retrieves the collection of ArbitreOfficielCategorie resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ArbitreOfficielCategorieApi()
page = 1  # int | The collection page number (optional) (default to 1)
officiel_categorie_id = 56  # int |  (optional)
officiel_categorie_id = [56]  # list[int] |  (optional)
cg_no = 56  # int |  (optional)
cg_no = [56]  # list[int] |  (optional)
officiel_categorie_sa_no = 56  # int |  (optional)
officiel_categorie_sa_no = [56]  # list[int] |  (optional)

try:
    # Retrieves the collection of ArbitreOfficielCategorie resources.
    api_response = api_instance.get_arbitre_officiel_categorie_collection(page=page,
                                                                          officiel_categorie_id=officiel_categorie_id,
                                                                          officiel_categorie_id=officiel_categorie_id,
                                                                          cg_no=cg_no, cg_no=cg_no,
                                                                          officiel_categorie_sa_no=officiel_categorie_sa_no,
                                                                          officiel_categorie_sa_no=officiel_categorie_sa_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArbitreOfficielCategorieApi->get_arbitre_officiel_categorie_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **officiel_categorie_id** | **int**|  | [optional] 
 **officiel_categorie_id** | [**list[int]**](int.md)|  | [optional] 
 **cg_no** | **int**|  | [optional] 
 **cg_no** | [**list[int]**](int.md)|  | [optional] 
 **officiel_categorie_sa_no** | **int**|  | [optional] 
 **officiel_categorie_sa_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitre_officiel_categorie_item**
> ArbitreOfficielCategorieJsonldOfficielCategorieArbitre get_arbitre_officiel_categorie_item(id)

Retrieves a ArbitreOfficielCategorie resource.

Retrieves a ArbitreOfficielCategorie resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ArbitreOfficielCategorieApi()
id = 'id_example'  # str | Resource identifier

try:
    # Retrieves a ArbitreOfficielCategorie resource.
    api_response = api_instance.get_arbitre_officiel_categorie_item(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArbitreOfficielCategorieApi->get_arbitre_officiel_categorie_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Resource identifier | 

### Return type

[**ArbitreOfficielCategorieJsonldOfficielCategorieArbitre**](ArbitreOfficielCategorieJsonldOfficielCategorieArbitre.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

