# PouleJsonldPouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfPouleJsonldPouleJourneeContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**gp_diff_no_tour** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

