# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ClubJsonldMatch(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'context': 'OneOfClubJsonldMatchContext',
        'id': 'str',
        'type': 'str',
        'cl_no': 'int',
        'external_updated_at': 'datetime',
        'logo': 'str'
    }

    attribute_map = {
        'context': '@context',
        'id': '@id',
        'type': '@type',
        'cl_no': 'cl_no',
        'external_updated_at': 'external_updated_at',
        'logo': 'logo'
    }

    def __init__(self, context=None, id=None, type=None, cl_no=None, external_updated_at=None, logo=None):  # noqa: E501
        """ClubJsonldMatch - a model defined in Swagger"""  # noqa: E501
        self._context = None
        self._id = None
        self._type = None
        self._cl_no = None
        self._external_updated_at = None
        self._logo = None
        self.discriminator = None
        if context is not None:
            self.context = context
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if cl_no is not None:
            self.cl_no = cl_no
        if external_updated_at is not None:
            self.external_updated_at = external_updated_at
        if logo is not None:
            self.logo = logo

    @property
    def context(self):
        """Gets the context of this ClubJsonldMatch.  # noqa: E501


        :return: The context of this ClubJsonldMatch.  # noqa: E501
        :rtype: OneOfClubJsonldMatchContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this ClubJsonldMatch.


        :param context: The context of this ClubJsonldMatch.  # noqa: E501
        :type: OneOfClubJsonldMatchContext
        """

        self._context = context

    @property
    def id(self):
        """Gets the id of this ClubJsonldMatch.  # noqa: E501


        :return: The id of this ClubJsonldMatch.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ClubJsonldMatch.


        :param id: The id of this ClubJsonldMatch.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this ClubJsonldMatch.  # noqa: E501


        :return: The type of this ClubJsonldMatch.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ClubJsonldMatch.


        :param type: The type of this ClubJsonldMatch.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def cl_no(self):
        """Gets the cl_no of this ClubJsonldMatch.  # noqa: E501


        :return: The cl_no of this ClubJsonldMatch.  # noqa: E501
        :rtype: int
        """
        return self._cl_no

    @cl_no.setter
    def cl_no(self, cl_no):
        """Sets the cl_no of this ClubJsonldMatch.


        :param cl_no: The cl_no of this ClubJsonldMatch.  # noqa: E501
        :type: int
        """

        self._cl_no = cl_no

    @property
    def external_updated_at(self):
        """Gets the external_updated_at of this ClubJsonldMatch.  # noqa: E501


        :return: The external_updated_at of this ClubJsonldMatch.  # noqa: E501
        :rtype: datetime
        """
        return self._external_updated_at

    @external_updated_at.setter
    def external_updated_at(self, external_updated_at):
        """Sets the external_updated_at of this ClubJsonldMatch.


        :param external_updated_at: The external_updated_at of this ClubJsonldMatch.  # noqa: E501
        :type: datetime
        """

        self._external_updated_at = external_updated_at

    @property
    def logo(self):
        """Gets the logo of this ClubJsonldMatch.  # noqa: E501


        :return: The logo of this ClubJsonldMatch.  # noqa: E501
        :rtype: str
        """
        return self._logo

    @logo.setter
    def logo(self, logo):
        """Sets the logo of this ClubJsonldMatch.


        :param logo: The logo of this ClubJsonldMatch.  # noqa: E501
        :type: str
        """

        self._logo = logo

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ClubJsonldMatch, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ClubJsonldMatch):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
