# swagger_client.IndividuApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_individu_item**](IndividuApi.md#get_individu_item) | **GET** /api/individus/{inNo} | Retrieves a Individu resource.

# **get_individu_item**
> IndividuJsonldIndividu get_individu_item(in_no)

Retrieves a Individu resource.

Retrieves a Individu resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.IndividuApi()
in_no = 'in_no_example'  # str | Resource identifier

try:
    # Retrieves a Individu resource.
    api_response = api_instance.get_individu_item(in_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IndividuApi->get_individu_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **in_no** | **str**| Resource identifier | 

### Return type

[**IndividuJsonldIndividu**](IndividuJsonldIndividu.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

