# PoulePouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**gp_diff_no_tour** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

