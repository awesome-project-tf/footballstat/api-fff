# swagger_client.MatchEntityApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_clubs_equipes_matchs_get_subresource_club_subresource**](MatchEntityApi.md#api_clubs_equipes_matchs_get_subresource_club_subresource) | **GET** /api/clubs/{clNo}/equipes/{equipes}/matchs | Retrieves a Club resource.
[**get_calendrier_of_club_match_entity_collection**](MatchEntityApi.md#get_calendrier_of_club_match_entity_collection) | **GET** /api/clubs/{clNo}/calendrier | Retrieves the collection of MatchEntity resources.
[**get_calendrier_of_competition_match_entity_collection**](MatchEntityApi.md#get_calendrier_of_competition_match_entity_collection) | **GET** /api/compets/{cpNo}/phases/{phNo}/poules/{gpNo}/calendrier | Retrieves the collection of MatchEntity resources.
[**get_calendrier_of_equipe_match_entity_collection**](MatchEntityApi.md#get_calendrier_of_equipe_match_entity_collection) | **GET** /api/clubs/{clNo}/equipes/{eqNo}/calendrier | Retrieves the collection of MatchEntity resources.
[**get_match_entity_collection**](MatchEntityApi.md#get_match_entity_collection) | **GET** /api/match_entities | Retrieves the collection of MatchEntity resources.
[**get_match_entity_item**](MatchEntityApi.md#get_match_entity_item) | **GET** /api/match_entities/{maNo} | Retrieves a MatchEntity resource.
[**get_matchs_of_club_match_entity_collection**](MatchEntityApi.md#get_matchs_of_club_match_entity_collection) | **GET** /api/clubs/{clNo}/matchs | Retrieves the collection of MatchEntity resources.
[**get_matchs_of_competition_match_entity_collection**](MatchEntityApi.md#get_matchs_of_competition_match_entity_collection) | **GET** /api/compets/{cpNo}/phases/{phNo}/poules/{gpNo}/matchs | Retrieves the collection of MatchEntity resources.
[**get_resultat_of_club_match_entity_collection**](MatchEntityApi.md#get_resultat_of_club_match_entity_collection) | **GET** /api/clubs/{clNo}/resultat | Retrieves the collection of MatchEntity resources.
[**get_resultat_of_competition_match_entity_collection**](MatchEntityApi.md#get_resultat_of_competition_match_entity_collection) | **GET** /api/compets/{cpNo}/phases/{phNo}/poules/{gpNo}/resultat | Retrieves the collection of MatchEntity resources.
[**get_resultat_of_equipe_match_entity_collection**](MatchEntityApi.md#get_resultat_of_equipe_match_entity_collection) | **GET** /api/clubs/{clNo}/equipes/{eqNo}/resultat | Retrieves the collection of MatchEntity resources.

# **api_clubs_equipes_matchs_get_subresource_club_subresource**
> InlineResponse2007 api_clubs_equipes_matchs_get_subresource_club_subresource(cl_no, equipes, page=page, cl_no=cl_no, competition_cp_no=competition_cp_no, competition_cp_no=competition_cp_no, ma_dat_before=ma_dat_before, ma_dat_strictly_before=ma_dat_strictly_before, ma_dat_after=ma_dat_after, ma_dat_strictly_after=ma_dat_strictly_after)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
cl_no = 'cl_no_example'  # str | Club identifier
equipes = 'equipes_example'  # str | Equipe identifier
page = 1  # int | The collection page number (optional) (default to 1)
cl_no = 56  # int |  (optional)
competition_cp_no = 56  # int |  (optional)
competition_cp_no = [56]  # list[int] |  (optional)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_strictly_before = 'ma_dat_strictly_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
ma_dat_strictly_after = 'ma_dat_strictly_after_example'  # str |  (optional)

try:
    # Retrieves a Club resource.
    api_response = api_instance.api_clubs_equipes_matchs_get_subresource_club_subresource(cl_no, equipes, page=page,
                                                                                          cl_no=cl_no,
                                                                                          competition_cp_no=competition_cp_no,
                                                                                          competition_cp_no=competition_cp_no,
                                                                                          ma_dat_before=ma_dat_before,
                                                                                          ma_dat_strictly_before=ma_dat_strictly_before,
                                                                                          ma_dat_after=ma_dat_after,
                                                                                          ma_dat_strictly_after=ma_dat_strictly_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->api_clubs_equipes_matchs_get_subresource_club_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Club identifier | 
 **equipes** | **str**| Equipe identifier | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_no** | **int**|  | [optional] 
 **competition_cp_no** | **int**|  | [optional] 
 **competition_cp_no** | [**list[int]**](int.md)|  | [optional] 
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_strictly_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **ma_dat_strictly_after** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calendrier_of_club_match_entity_collection**
> InlineResponse2007 get_calendrier_of_club_match_entity_collection(, page=page)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  # | 
page = 1  # int | The collection page number (optional) (default to 1)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_calendrier_of_club_match_entity_collection(, page = page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_calendrier_of_club_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calendrier_of_competition_match_entity_collection**
> InlineResponse2007 get_calendrier_of_competition_match_entity_collection(, , , page=page, ma_dat_before=ma_dat_before, ma_dat_after=ma_dat_after, cl_no=cl_no)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  # | 
= apifff.null()  # | 
= apifff.null()  # | 
page = 1  # int | The collection page number (optional) (default to 1)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
cl_no = 'cl_no_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_calendrier_of_competition_match_entity_collection(,, , page = page, ma_dat_before = ma_dat_before, ma_dat_after = ma_dat_after, cl_no = cl_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_calendrier_of_competition_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **cl_no** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calendrier_of_equipe_match_entity_collection**
> InlineResponse2007 get_calendrier_of_equipe_match_entity_collection(, , page=page, ma_dat_before=ma_dat_before, ma_dat_after=ma_dat_after, cp_no=cp_no)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  # | 
= apifff.null()  # | 
page = 1  # int | The collection page number (optional) (default to 1)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
cp_no = 56  # int |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_calendrier_of_equipe_match_entity_collection(,, page = page, ma_dat_before = ma_dat_before, ma_dat_after = ma_dat_after, cp_no = cp_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_calendrier_of_equipe_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **cp_no** | **int**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_match_entity_collection**
> InlineResponse2007 get_match_entity_collection(page=page, cl_no=cl_no, competition_cp_no=competition_cp_no, competition_cp_no=competition_cp_no, ma_dat_before=ma_dat_before, ma_dat_strictly_before=ma_dat_strictly_before, ma_dat_after=ma_dat_after, ma_dat_strictly_after=ma_dat_strictly_after)

Retrieves the collection of MatchEntity resources.

Permet de récupérer les matchs, /!\\ toujours passer des dates en filtre sinon timeout !!!

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
page = 1  # int | The collection page number (optional) (default to 1)
cl_no = 56  # int |  (optional)
competition_cp_no = 56  # int |  (optional)
competition_cp_no = [56]  # list[int] |  (optional)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_strictly_before = 'ma_dat_strictly_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
ma_dat_strictly_after = 'ma_dat_strictly_after_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_match_entity_collection(page=page, cl_no=cl_no, competition_cp_no=competition_cp_no,
                                                            competition_cp_no=competition_cp_no,
                                                            ma_dat_before=ma_dat_before,
                                                            ma_dat_strictly_before=ma_dat_strictly_before,
                                                            ma_dat_after=ma_dat_after,
                                                            ma_dat_strictly_after=ma_dat_strictly_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_no** | **int**|  | [optional] 
 **competition_cp_no** | **int**|  | [optional] 
 **competition_cp_no** | [**list[int]**](int.md)|  | [optional] 
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_strictly_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **ma_dat_strictly_after** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_match_entity_item**
> MatchEntityJsonldMatch get_match_entity_item(ma_no)

Retrieves a MatchEntity resource.

Retrieves a MatchEntity resource.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
ma_no = 'ma_no_example'  # str | Resource identifier

try:
    # Retrieves a MatchEntity resource.
    api_response = api_instance.get_match_entity_item(ma_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_match_entity_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ma_no** | **str**| Resource identifier | 

### Return type

[**MatchEntityJsonldMatch**](MatchEntityJsonldMatch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_matchs_of_club_match_entity_collection**
> InlineResponse2007 get_matchs_of_club_match_entity_collection(, page=page, ma_dat_before=ma_dat_before, ma_dat_after=ma_dat_after)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  # | 
page = 1  # int | The collection page number (optional) (default to 1)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_matchs_of_club_match_entity_collection(, page = page, ma_dat_before = ma_dat_before, ma_dat_after = ma_dat_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_matchs_of_club_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_matchs_of_competition_match_entity_collection**
> InlineResponse2007 get_matchs_of_competition_match_entity_collection(, , , , cl_no=cl_no, =, page=page, cl_no=cl_no, competition_cp_no=competition_cp_no, competition_cp_no=competition_cp_no, ma_dat_before=ma_dat_before, ma_dat_strictly_before=ma_dat_strictly_before, ma_dat_after=ma_dat_after, ma_dat_strictly_after=ma_dat_strictly_after)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  # | 
= apifff.null()  # | 
= apifff.null()  # | 
= apifff.null()  # | 
cl_no = 'cl_no_example'  # str |  (optional)
= apifff.null()  # | pjNo de la pouleJournée (optional)
page = 1  # int | The collection page number (optional) (default to 1)
cl_no = 56  # int |  (optional)
competition_cp_no = 56  # int |  (optional)
competition_cp_no = [56]  # list[int] |  (optional)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_strictly_before = 'ma_dat_strictly_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
ma_dat_strictly_after = 'ma_dat_strictly_after_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_matchs_of_competition_match_entity_collection(,, , , cl_no = cl_no, =, page = page, cl_no = cl_no, competition_cp_no = competition_cp_no, competition_cp_no = competition_cp_no, ma_dat_before = ma_dat_before, ma_dat_strictly_before = ma_dat_strictly_before, ma_dat_after = ma_dat_after, ma_dat_strictly_after = ma_dat_strictly_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_matchs_of_competition_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **cl_no** | **str**|  | [optional] 
 **** | [****](.md)| pjNo de la pouleJournée | [optional] 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_no** | **int**|  | [optional] 
 **competition_cp_no** | **int**|  | [optional] 
 **competition_cp_no** | [**list[int]**](int.md)|  | [optional] 
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_strictly_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **ma_dat_strictly_after** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_resultat_of_club_match_entity_collection**
> InlineResponse2007 get_resultat_of_club_match_entity_collection(, page=page)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  #  | 
page = 1  # int | The collection page number (optional) (default to 1)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_resultat_of_club_match_entity_collection(, page = page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_resultat_of_club_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_resultat_of_competition_match_entity_collection**
> InlineResponse2007 get_resultat_of_competition_match_entity_collection(, , , =, page=page, ma_dat_before=ma_dat_before, ma_dat_after=ma_dat_after, cl_no=cl_no)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  #  | 
= apifff.null()  #  | 
= apifff.null()  #  | 
= apifff.null()  #  | pjNo de la pouleJournée (optional)
page = 1  # int | The collection page number (optional) (default to 1)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
cl_no = 'cl_no_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_resultat_of_competition_match_entity_collection(,, , =, page = page, ma_dat_before = ma_dat_before, ma_dat_after = ma_dat_after, cl_no = cl_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_resultat_of_competition_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **** | [****](.md)| pjNo de la pouleJournée | [optional] 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **cl_no** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_resultat_of_equipe_match_entity_collection**
> InlineResponse2007 get_resultat_of_equipe_match_entity_collection(, , page=page, ma_dat_before=ma_dat_before, ma_dat_after=ma_dat_after, cp_no=cp_no)

Retrieves the collection of MatchEntity resources.

Retrieves the collection of MatchEntity resources.

### Example
```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchEntityApi()
= apifff.null()  #  | 
= apifff.null()  #  | 
page = 1  # int | The collection page number (optional) (default to 1)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
cp_no = 56  # int |  (optional)

try:
    # Retrieves the collection of MatchEntity resources.
    api_response = api_instance.get_resultat_of_equipe_match_entity_collection(,, page = page, ma_dat_before = ma_dat_before, ma_dat_after = ma_dat_after, cp_no = cp_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchEntityApi->get_resultat_of_equipe_match_entity_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **cp_no** | **int**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

