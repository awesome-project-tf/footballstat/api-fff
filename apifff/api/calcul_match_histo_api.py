# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from apifff.api_client import ApiClient


class CalculMatchHistoApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_calcul_match_histo_collection(self, **kwargs):  # noqa: E501
        """Retrieves the collection of CalculMatchHisto resources.  # noqa: E501

        Retrieves the collection of CalculMatchHisto resources.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_calcul_match_histo_collection(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int page: The collection page number
        :param list[int] cl_no1:
        :param list[int] cl_no2:
        :param list[str] lc_cod1:
        :param list[int] eq_cod1:
        :param list[str] lc_cod2:
        :param list[int] eq_cod2:
        :return: InlineResponse2003
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_calcul_match_histo_collection_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_calcul_match_histo_collection_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_calcul_match_histo_collection_with_http_info(self, **kwargs):  # noqa: E501
        """Retrieves the collection of CalculMatchHisto resources.  # noqa: E501

        Retrieves the collection of CalculMatchHisto resources.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_calcul_match_histo_collection_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int page: The collection page number
        :param list[int] cl_no1:
        :param list[int] cl_no2:
        :param list[str] lc_cod1:
        :param list[int] eq_cod1:
        :param list[str] lc_cod2:
        :param list[int] eq_cod2:
        :return: InlineResponse2003
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['page', 'cl_no1', 'cl_no2', 'lc_cod1', 'eq_cod1', 'lc_cod2', 'eq_cod2']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_calcul_match_histo_collection" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'page' in params:
            query_params.append(('page', params['page']))  # noqa: E501
        if 'cl_no1' in params:
            query_params.append(('clNo1', params['cl_no1']))  # noqa: E501
            collection_formats['clNo1'] = 'multi'  # noqa: E501
        if 'cl_no2' in params:
            query_params.append(('clNo2', params['cl_no2']))  # noqa: E501
            collection_formats['clNo2'] = 'multi'  # noqa: E501
        if 'lc_cod1' in params:
            query_params.append(('lcCod1', params['lc_cod1']))  # noqa: E501
            collection_formats['lcCod1'] = 'multi'  # noqa: E501
        if 'eq_cod1' in params:
            query_params.append(('eqCod1', params['eq_cod1']))  # noqa: E501
            collection_formats['eqCod1'] = 'multi'  # noqa: E501
        if 'lc_cod2' in params:
            query_params.append(('lcCod2', params['lc_cod2']))  # noqa: E501
            collection_formats['lcCod2'] = 'multi'  # noqa: E501
        if 'eq_cod2' in params:
            query_params.append(('eqCod2', params['eq_cod2']))  # noqa: E501
            collection_formats['eqCod2'] = 'multi'  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/ld+json', 'application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/api/calcul_match_histos', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse2003',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_calcul_match_histo_item(self, id, **kwargs):  # noqa: E501
        """Retrieves a CalculMatchHisto resource.  # noqa: E501

        Retrieves a CalculMatchHisto resource.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_calcul_match_histo_item(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Resource identifier (required)
        :return: CalculMatchHistoJsonldCalculMatchHisto
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_calcul_match_histo_item_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_calcul_match_histo_item_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def get_calcul_match_histo_item_with_http_info(self, id, **kwargs):  # noqa: E501
        """Retrieves a CalculMatchHisto resource.  # noqa: E501

        Retrieves a CalculMatchHisto resource.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_calcul_match_histo_item_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Resource identifier (required)
        :return: CalculMatchHistoJsonldCalculMatchHisto
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_calcul_match_histo_item" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params or
                params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_calcul_match_histo_item`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/ld+json', 'application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/api/calcul_match_histos/{id}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='CalculMatchHistoJsonldCalculMatchHisto',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
