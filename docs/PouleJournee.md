# PouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**pj_ar** | **str** |  | [optional] 
**pj_no_tour** | **int** |  | [optional] 
**pj_lib_tour** | **str** |  | [optional] 
**pj_dat_class_auto** | **datetime** |  | [optional] 
**pj_dat_class_offi** | **datetime** |  | [optional] 
**poule** | **str** |  | [optional] 
**phase** | **str** |  | [optional] 
**competition** | **str** |  | [optional] 
**matchs** | **list[str]** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**id** | **int** |  | [optional] 
**external_message_id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**at_least_one_match_resultat** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

