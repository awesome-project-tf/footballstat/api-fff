# ClubJsonldClassement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfClubJsonldClassementContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**cl_no** | **int** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

