# EquipeJsonldMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfEquipeJsonldMatchContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**club** | **AnyOfEquipeJsonldMatchClub** |  | [optional] 
**category_code** | **str** |  | [optional] 
**number** | **int** |  | [optional] 
**code** | **int** |  | [optional] 
**short_name** | **str** |  | [optional] 
**short_name_ligue** | **str** |  | [optional] 
**short_name_federation** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**engagements** | [**list[EngagementJsonldMatch]**](EngagementJsonldMatch.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**category_label** | **str** |  | [optional] 
**category_gender** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

