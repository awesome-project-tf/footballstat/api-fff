# PouleJsonldPoule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfPouleJsonldPouleContext** |  | [optional] 
**stage_number** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**code** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**gp_diff_info** | **str** |  | [optional] 
**gp_diff_no_tour** | **int** |  | [optional] 
**phase** | [**PhaseJsonldPoule**](PhaseJsonldPoule.md) |  | [optional] 
**competition** | [**CompetJsonldPoule**](CompetJsonldPoule.md) |  | [optional] 
**cdg** | [**CdgJsonldPoule**](CdgJsonldPoule.md) |  | [optional] 
**rounds** | [**list[PouleJourneeJsonldPoule]**](PouleJourneeJsonldPoule.md) |  | [optional] 
**poule_unique** | **bool** |  | [optional] 
**at_least_one_match_resultat** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

