# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.one_of_phase_jsonld_compet_context import OneOfPhaseJsonldCompetContext  # noqa: E501
from apifff.rest import ApiException


class TestOneOfPhaseJsonldCompetContext(unittest.TestCase):
    """OneOfPhaseJsonldCompetContext unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOneOfPhaseJsonldCompetContext(self):
        """Test OneOfPhaseJsonldCompetContext"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.one_of_phase_jsonld_compet_context.OneOfPhaseJsonldCompetContext()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
