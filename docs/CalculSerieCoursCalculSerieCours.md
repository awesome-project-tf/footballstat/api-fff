# CalculSerieCoursCalculSerieCours

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**goal_scored** | **int** |  | [optional] 
**goal_conceded** | **int** |  | [optional] 
**day1** | **str** |  | [optional] 
**day2** | **str** |  | [optional] 
**day3** | **str** |  | [optional] 
**day4** | **str** |  | [optional] 
**day5** | **str** |  | [optional] 
**match1** | **AnyOfCalculSerieCoursCalculSerieCoursMatch1** |  | [optional] 
**match2** | **AnyOfCalculSerieCoursCalculSerieCoursMatch2** |  | [optional] 
**match3** | **AnyOfCalculSerieCoursCalculSerieCoursMatch3** |  | [optional] 
**match4** | **AnyOfCalculSerieCoursCalculSerieCoursMatch4** |  | [optional] 
**match5** | **AnyOfCalculSerieCoursCalculSerieCoursMatch5** |  | [optional] 
**nb_match** | **int** |  | [optional] 
**nb_match_win** | **int** |  | [optional] 
**nb_match_lost** | **int** |  | [optional] 
**nb_match_equal** | **int** |  | [optional] 
**nb_win_home** | **int** |  | [optional] 
**nb_lost_home** | **int** |  | [optional] 
**nb_equal_home** | **int** |  | [optional] 
**nb_win_away** | **int** |  | [optional] 
**nb_lost_away** | **int** |  | [optional] 
**nb_equal_away** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

