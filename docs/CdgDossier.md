# CdgDossier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cg_no** | **int** |  | [optional] 
**type_label** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**cg_no_supp** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

