# MatchEntityJsonldCalculSerieCours

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchEntityJsonldCalculSerieCoursContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**competition** | **AnyOfMatchEntityJsonldCalculSerieCoursCompetition** |  | [optional] 
**poule** | **AnyOfMatchEntityJsonldCalculSerieCoursPoule** |  | [optional] 
**poule_journee** | **AnyOfMatchEntityJsonldCalculSerieCoursPouleJournee** |  | [optional] 
**home** | **AnyOfMatchEntityJsonldCalculSerieCoursHome** |  | [optional] 
**away** | **AnyOfMatchEntityJsonldCalculSerieCoursAway** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**time** | **str** |  | [optional] 
**cr_nb_but** | **int** |  | [optional] 
**home_score** | **int** |  | [optional] 
**away_nb_but** | **int** |  | [optional] 
**away_score** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

