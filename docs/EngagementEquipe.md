# EngagementEquipe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sa_no** | **int** |  | [optional] 
**terrain** | **str** |  | [optional] 
**en_statut** | **str** |  | [optional] 
**en_forf_gene** | **str** |  | [optional] 
**en_tour_no** | **int** |  | [optional] 
**en_elimine** | **str** |  | [optional] 
**en_excl_class** | **str** |  | [optional] 
**competition** | [**CompetEquipe**](CompetEquipe.md) |  | [optional] 
**phase** | [**PhaseEquipe**](PhaseEquipe.md) |  | [optional] 
**poule** | [**PouleEquipe**](PouleEquipe.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

