# DossierDecisionJsonldDossier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfDossierDecisionJsonldDossierContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**dd_no** | **int** |  | [optional] 
**cc_cod** | **str** |  | [optional] 
**dd_statut** | **str** |  | [optional] 
**dd_dat_effet** | **datetime** |  | [optional] 
**dd_dat_fin** | **datetime** |  | [optional] 
**dd_dat_delai_recid** | **datetime** |  | [optional] 
**dd_a_vie** | **str** |  | [optional] 
**dd_lib_motif** | **str** |  | [optional] 
**dd_lib_decis** | **str** |  | [optional] 
**dd_epur** | **str** |  | [optional] 
**dd_purg** | **str** |  | [optional] 
**dd_niv** | **int** |  | [optional] 
**lb_no** | **int** |  | [optional] 
**dc_diff_info** | **str** |  | [optional] 
**lb_dat** | **datetime** |  | [optional] 
**fc_mt** | **str** |  | [optional] 
**cr_dat** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

