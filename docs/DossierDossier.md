# DossierDossier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ds_no** | **str** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**in_no** | **str** |  | [optional] 
**li_no** | **str** |  | [optional] 
**td_cod** | **str** |  | [optional] 
**dec_no** | **str** |  | [optional] 
**ds_dat** | **datetime** |  | [optional] 
**ds_lib_motif** | **str** |  | [optional] 
**ds_sujet** | **str** |  | [optional] 
**ds_nb_jaune** | **int** |  | [optional] 
**ds_rouge** | **str** |  | [optional] 
**ds_hors_match** | **str** |  | [optional] 
**in_nom** | **str** |  | [optional] 
**in_prenom** | **str** |  | [optional] 
**in_civ** | **str** |  | [optional] 
**cdg** | [**CdgDossier**](CdgDossier.md) |  | [optional] 
**match_entity** | **AnyOfDossierDossierMatchEntity** |  | [optional] 
**equipe** | **AnyOfDossierDossierEquipe** |  | [optional] 
**decisions** | [**list[DossierDecisionDossier]**](DossierDecisionDossier.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

