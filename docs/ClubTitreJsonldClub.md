# ClubTitreJsonldClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfClubTitreJsonldClubContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**individu** | [**IndividuJsonldClub**](IndividuJsonldClub.md) |  | [optional] 
**in_nom** | **str** |  | [optional] 
**in_prenom** | **str** |  | [optional] 
**ti_lib** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

