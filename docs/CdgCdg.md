# CdgCdg

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cg_no** | **int** |  | [optional] 
**cg_cod** | **int** |  | [optional] 
**type_label** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**short_name** | **str** |  | [optional] 
**cg_no_supp** | **int** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**cp_cod** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

