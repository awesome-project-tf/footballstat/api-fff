# TerrainJsonldMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfTerrainJsonldMatchContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**te_no** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**zip_code** | **str** |  | [optional] 
**city** | **str** |  | [optional] 
**libelle_surface** | **str** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**address** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

