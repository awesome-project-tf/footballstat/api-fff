# CompetMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cp_no** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgMatch**](CdgMatch.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

