# MatchFeuilleEvenementJsonldMatchFeuille

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchFeuilleEvenementJsonldMatchFeuilleContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**cfe_no** | **int** |  | [optional] 
**l1_li_no** | **AnyOfMatchFeuilleEvenementJsonldMatchFeuilleL1LiNo** |  | [optional] 
**l2_li_no** | **AnyOfMatchFeuilleEvenementJsonldMatchFeuilleL2LiNo** |  | [optional] 
**l1_cfi_maillot** | **int** |  | [optional] 
**l2_cfi_maillot** | **int** |  | [optional] 
**cfe_typ** | **str** |  | [optional] 
**cfe_min** | **int** |  | [optional] 
**cfe_min_sup** | **int** |  | [optional] 
**cfe_typ_but** | **str** |  | [optional] 
**cfe_action** | **str** |  | [optional] 
**cfe_cod_motif** | **str** |  | [optional] 
**cfe_cod_bless** | **str** |  | [optional] 
**cfe_lib** | **str** |  | [optional] 
**cfe_trib** | **str** |  | [optional] 
**cfe_denombrement** | **str** |  | [optional] 
**cfe_tribune** | **str** |  | [optional] 
**cfe_nb_evmt** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

