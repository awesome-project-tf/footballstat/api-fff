# CalculMatchHistoCalculMatchHisto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nb_ma_win** | **int** |  | [optional] 
**nb_ma_win_eq2** | **int** |  | [optional] 
**nb_ma_equal** | **int** |  | [optional] 
**match1** | [**MatchEntityCalculMatchHisto**](MatchEntityCalculMatchHisto.md) |  | [optional] 
**match2** | **AnyOfCalculMatchHistoCalculMatchHistoMatch2** |  | [optional] 
**match3** | **AnyOfCalculMatchHistoCalculMatchHistoMatch3** |  | [optional] 
**match4** | **AnyOfCalculMatchHistoCalculMatchHistoMatch4** |  | [optional] 
**match5** | **AnyOfCalculMatchHistoCalculMatchHistoMatch5** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

