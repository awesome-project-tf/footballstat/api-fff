# MatchMembreMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mm_no** | **int** |  | [optional] 
**match_entity** | **AnyOfMatchMembreMatchMembreMatchEntity** |  | [optional] 
**in_no** | **str** |  | [optional] 
**po_cod** | **str** |  | [optional] 
**mm_nb_km** | **int** |  | [optional] 
**cdg** | [**CdgMatchMembre**](CdgMatchMembre.md) |  | [optional] 
**prenom** | **str** |  | [optional] 
**nom** | **str** |  | [optional] 
**label_position** | **str** |  | [optional] 
**position_ordre** | **int** |  | [optional] 
**in_no_obs** | **str** |  | [optional] 
**ctr_cod** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

