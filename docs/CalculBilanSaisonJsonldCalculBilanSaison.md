# CalculBilanSaisonJsonldCalculBilanSaison

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfCalculBilanSaisonJsonldCalculBilanSaisonContext** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**nb_team** | **int** |  | [optional] 
**ranking_evol** | **str** |  | [optional] 
**nb_match** | **int** |  | [optional] 
**nb_win_home** | **int** |  | [optional] 
**nb_equal_home** | **int** |  | [optional] 
**nb_lost_home** | **int** |  | [optional] 
**nb_win_away** | **int** |  | [optional] 
**nb_equal_away** | **int** |  | [optional] 
**nb_lost_away** | **int** |  | [optional] 
**goal_scored** | **int** |  | [optional] 
**average_goal_scored** | **float** |  | [optional] 
**goal_conceded** | **int** |  | [optional] 
**average_goal_conceded** | **float** |  | [optional] 
**best_match** | **AnyOfCalculBilanSaisonJsonldCalculBilanSaisonBestMatch** |  | [optional] 
**worst_match** | **AnyOfCalculBilanSaisonJsonldCalculBilanSaisonWorstMatch** |  | [optional] 
**team_final_position** | **str** |  | [optional] 
**equipe** | **AnyOfCalculBilanSaisonJsonldCalculBilanSaisonEquipe** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

