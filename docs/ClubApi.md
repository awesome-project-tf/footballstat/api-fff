# swagger_client.ClubApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_clubs_equipes_get_subresource_club_subresource**](ClubApi.md#api_clubs_equipes_get_subresource_club_subresource) | **GET** /api/clubs/{clNo}/equipes | Retrieves a Club resource.
[**get_club_collection**](ClubApi.md#get_club_collection) | **GET** /api/clubs | Retrieves the collection of Club resources.
[**get_club_item**](ClubApi.md#get_club_item) | **GET** /api/clubs/{clNo} | Retrieves a Club resource.

# **api_clubs_equipes_get_subresource_club_subresource**
> InlineResponse2008 api_clubs_equipes_get_subresource_club_subresource(cl_no, page=page, eq_no=eq_no, eq_no=eq_no)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ClubApi()
cl_no = 'cl_no_example'  # str | Club identifier
page = 1  # int | The collection page number (optional) (default to 1)
eq_no = 56  # int |  (optional)
eq_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Club resource.
    api_response = api_instance.api_clubs_equipes_get_subresource_club_subresource(cl_no, page=page, eq_no=eq_no,
                                                                                   eq_no=eq_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClubApi->api_clubs_equipes_get_subresource_club_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Club identifier | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **eq_no** | **int**|  | [optional] 
 **eq_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_club_collection**
> InlineResponse2006 get_club_collection(page=page, cl_cp=cl_cp, cl_cp=cl_cp, cdg_cg_no=cdg_cg_no, cdg_cg_no=cdg_cg_no, cl_cod=cl_cod, cl_cod=cl_cod, cl_nom=cl_nom, order_cl_cp=order_cl_cp)

Retrieves the collection of Club resources.

Retrieves the collection of Club resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ClubApi()
page = 1  # int | The collection page number (optional) (default to 1)
cl_cp = 'cl_cp_example'  # str |  (optional)
cl_cp = ['cl_cp_example']  # list[str] |  (optional)
cdg_cg_no = 56  # int |  (optional)
cdg_cg_no = [56]  # list[int] |  (optional)
cl_cod = 56  # int |  (optional)
cl_cod = [56]  # list[int] |  (optional)
cl_nom = 'cl_nom_example'  # str |  (optional)
order_cl_cp = 'order_cl_cp_example'  # str |  (optional)

try:
    # Retrieves the collection of Club resources.
    api_response = api_instance.get_club_collection(page=page, cl_cp=cl_cp, cl_cp=cl_cp, cdg_cg_no=cdg_cg_no,
                                                    cdg_cg_no=cdg_cg_no, cl_cod=cl_cod, cl_cod=cl_cod, cl_nom=cl_nom,
                                                    order_cl_cp=order_cl_cp)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClubApi->get_club_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_cp** | **str**|  | [optional] 
 **cl_cp** | [**list[str]**](str.md)|  | [optional] 
 **cdg_cg_no** | **int**|  | [optional] 
 **cdg_cg_no** | [**list[int]**](int.md)|  | [optional] 
 **cl_cod** | **int**|  | [optional] 
 **cl_cod** | [**list[int]**](int.md)|  | [optional] 
 **cl_nom** | **str**|  | [optional] 
 **order_cl_cp** | **str**|  | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_club_item**
> ClubJsonldClub get_club_item(cl_no)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ClubApi()
cl_no = 'cl_no_example'  # str | Resource identifier

try:
    # Retrieves a Club resource.
    api_response = api_instance.get_club_item(cl_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ClubApi->get_club_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Resource identifier | 

### Return type

[**ClubJsonldClub**](ClubJsonldClub.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

