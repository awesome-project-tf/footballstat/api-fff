# ArbitreOfficielCategorieJsonldOfficielCategorieArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfArbitreOfficielCategorieJsonldOfficielCategorieArbitreContext** |  | [optional] 
**arbitre** | [**ArbitreJsonldOfficielCategorieArbitre**](ArbitreJsonldOfficielCategorieArbitre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

