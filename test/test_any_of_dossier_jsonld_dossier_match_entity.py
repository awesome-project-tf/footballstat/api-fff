# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_dossier_jsonld_dossier_match_entity import AnyOfDossierJsonldDossierMatchEntity  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfDossierJsonldDossierMatchEntity(unittest.TestCase):
    """AnyOfDossierJsonldDossierMatchEntity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfDossierJsonldDossierMatchEntity(self):
        """Test AnyOfDossierJsonldDossierMatchEntity"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_dossier_jsonld_dossier_match_entity.AnyOfDossierJsonldDossierMatchEntity()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
