# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.api.calcul_serie_cours_api import CalculSerieCoursApi  # noqa: E501
from apifff.rest import ApiException


class TestCalculSerieCoursApi(unittest.TestCase):
    """CalculSerieCoursApi unit test stubs"""

    def setUp(self):
        self.api = CalculSerieCoursApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_calcul_serie_cours_collection(self):
        """Test case for get_calcul_serie_cours_collection

        Retrieves the collection of CalculSerieCours resources.  # noqa: E501
        """
        pass

    def test_get_calcul_serie_cours_item(self):
        """Test case for get_calcul_serie_cours_item

        Retrieves a CalculSerieCours resource.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
