# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.api.officiel_categorie_api import OfficielCategorieApi  # noqa: E501
from apifff.rest import ApiException


class TestOfficielCategorieApi(unittest.TestCase):
    """OfficielCategorieApi unit test stubs"""

    def setUp(self):
        self.api = OfficielCategorieApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_officiel_categorie_collection(self):
        """Test case for get_officiel_categorie_collection

        Retrieves the collection of OfficielCategorie resources.  # noqa: E501
        """
        pass

    def test_get_officiel_categorie_item(self):
        """Test case for get_officiel_categorie_item

        Retrieves a OfficielCategorie resource.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
