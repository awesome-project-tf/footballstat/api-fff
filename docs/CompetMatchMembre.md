# CompetMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cp_no** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgMatchMembre**](CdgMatchMembre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

