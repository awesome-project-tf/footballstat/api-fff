# PouleJourneePouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**pj_lib_tour** | **str** |  | [optional] 
**poule** | [**PoulePouleJournee**](PoulePouleJournee.md) |  | [optional] 
**phase** | [**PhasePouleJournee**](PhasePouleJournee.md) |  | [optional] 
**competition** | [**CompetPouleJournee**](CompetPouleJournee.md) |  | [optional] 
**name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

