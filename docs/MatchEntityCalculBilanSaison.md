# MatchEntityCalculBilanSaison

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competition** | **AnyOfMatchEntityCalculBilanSaisonCompetition** |  | [optional] 
**poule** | **AnyOfMatchEntityCalculBilanSaisonPoule** |  | [optional] 
**poule_journee** | **AnyOfMatchEntityCalculBilanSaisonPouleJournee** |  | [optional] 
**home** | **AnyOfMatchEntityCalculBilanSaisonHome** |  | [optional] 
**away** | **AnyOfMatchEntityCalculBilanSaisonAway** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**time** | **str** |  | [optional] 
**cr_nb_but** | **int** |  | [optional] 
**home_score** | **int** |  | [optional] 
**away_nb_but** | **int** |  | [optional] 
**away_score** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

