# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_match_entity_jsonld_calcul_match_histo_home import AnyOfMatchEntityJsonldCalculMatchHistoHome  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfMatchEntityJsonldCalculMatchHistoHome(unittest.TestCase):
    """AnyOfMatchEntityJsonldCalculMatchHistoHome unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfMatchEntityJsonldCalculMatchHistoHome(self):
        """Test AnyOfMatchEntityJsonldCalculMatchHistoHome"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_match_entity_jsonld_calcul_match_histo_home.AnyOfMatchEntityJsonldCalculMatchHistoHome()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
