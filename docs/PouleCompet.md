# PouleCompet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**cdg** | [**CdgCompet**](CdgCompet.md) |  | [optional] 
**diffusable** | **bool** |  | [optional] 
**matchs** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

