# PouleClassement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**cdg** | [**CdgClassement**](CdgClassement.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**poule_unique** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

