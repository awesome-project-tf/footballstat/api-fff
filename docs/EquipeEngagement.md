# EquipeEngagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**club** | **AnyOfEquipeEngagementClub** |  | [optional] 
**category_code** | **str** |  | [optional] 
**number** | **int** |  | [optional] 
**code** | **int** |  | [optional] 
**short_name** | **str** |  | [optional] 
**short_name_ligue** | **str** |  | [optional] 
**short_name_federation** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**category_label** | **str** |  | [optional] 
**category_gender** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

