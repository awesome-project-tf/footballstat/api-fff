# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class CompetEquipe(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'cp_no': 'int',
        'type': 'str',
        'name': 'str',
        'level': 'str'
    }

    attribute_map = {
        'cp_no': 'cp_no',
        'type': 'type',
        'name': 'name',
        'level': 'level'
    }

    def __init__(self, cp_no=None, type=None, name=None, level=None):  # noqa: E501
        """CompetEquipe - a model defined in Swagger"""  # noqa: E501
        self._cp_no = None
        self._type = None
        self._name = None
        self._level = None
        self.discriminator = None
        if cp_no is not None:
            self.cp_no = cp_no
        if type is not None:
            self.type = type
        if name is not None:
            self.name = name
        if level is not None:
            self.level = level

    @property
    def cp_no(self):
        """Gets the cp_no of this CompetEquipe.  # noqa: E501


        :return: The cp_no of this CompetEquipe.  # noqa: E501
        :rtype: int
        """
        return self._cp_no

    @cp_no.setter
    def cp_no(self, cp_no):
        """Sets the cp_no of this CompetEquipe.


        :param cp_no: The cp_no of this CompetEquipe.  # noqa: E501
        :type: int
        """

        self._cp_no = cp_no

    @property
    def type(self):
        """Gets the type of this CompetEquipe.  # noqa: E501


        :return: The type of this CompetEquipe.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this CompetEquipe.


        :param type: The type of this CompetEquipe.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def name(self):
        """Gets the name of this CompetEquipe.  # noqa: E501


        :return: The name of this CompetEquipe.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this CompetEquipe.


        :param name: The name of this CompetEquipe.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def level(self):
        """Gets the level of this CompetEquipe.  # noqa: E501


        :return: The level of this CompetEquipe.  # noqa: E501
        :rtype: str
        """
        return self._level

    @level.setter
    def level(self, level):
        """Sets the level of this CompetEquipe.


        :param level: The level of this CompetEquipe.  # noqa: E501
        :type: str
        """

        self._level = level

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(CompetEquipe, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CompetEquipe):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
