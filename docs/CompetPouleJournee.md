# CompetPouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cp_no** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**level** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

