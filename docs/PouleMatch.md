# PouleMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**poule_unique** | **bool** |  | [optional] 
**at_least_one_match_resultat** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

