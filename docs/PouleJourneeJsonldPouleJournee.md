# PouleJourneeJsonldPouleJournee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfPouleJourneeJsonldPouleJourneeContext** |  | [optional] 
**number** | **int** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**pj_lib_tour** | **str** |  | [optional] 
**poule** | [**PouleJsonldPouleJournee**](PouleJsonldPouleJournee.md) |  | [optional] 
**phase** | [**PhaseJsonldPouleJournee**](PhaseJsonldPouleJournee.md) |  | [optional] 
**competition** | [**CompetJsonldPouleJournee**](CompetJsonldPouleJournee.md) |  | [optional] 
**name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

