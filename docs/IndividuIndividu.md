# IndividuIndividu

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in_no** | **str** |  | [optional] 
**in_nom** | **str** |  | [optional] 
**in_prenom** | **str** |  | [optional] 
**in_sexe** | **str** |  | [optional] 
**in_civ** | **str** |  | [optional] 
**in_diff_adr** | **str** |  | [optional] 
**in_dat_nais** | **datetime** |  | [optional] 
**contacts** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

