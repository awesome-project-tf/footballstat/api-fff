# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.cdg_match import CdgMatch  # noqa: E501
from apifff.rest import ApiException


class TestCdgMatch(unittest.TestCase):
    """CdgMatch unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCdgMatch(self):
        """Test CdgMatch"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.cdg_match.CdgMatch()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
