# swagger_client.MatchFeuilleApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_match_feuille_collection**](MatchFeuilleApi.md#get_match_feuille_collection) | **GET** /api/match_feuilles | Retrieves the collection of MatchFeuille resources.
[**get_match_feuille_item**](MatchFeuilleApi.md#get_match_feuille_item) | **GET** /api/match_feuilles/{cfmNo} | Retrieves a MatchFeuille resource.

# **get_match_feuille_collection**
> InlineResponse20016 get_match_feuille_collection(match_entity_ma_no=match_entity_ma_no, match_entity_ma_no=match_entity_ma_no)

Retrieves the collection of MatchFeuille resources.

Retrieves the collection of MatchFeuille resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchFeuilleApi()
match_entity_ma_no = 56  # int |  (optional)
match_entity_ma_no = [56]  # list[int] |  (optional)

try:
    # Retrieves the collection of MatchFeuille resources.
    api_response = api_instance.get_match_feuille_collection(match_entity_ma_no=match_entity_ma_no,
                                                             match_entity_ma_no=match_entity_ma_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchFeuilleApi->get_match_feuille_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **match_entity_ma_no** | **int**|  | [optional] 
 **match_entity_ma_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_match_feuille_item**
> MatchFeuilleJsonldMatchFeuille get_match_feuille_item(cfm_no)

Retrieves a MatchFeuille resource.

Retrieves a MatchFeuille resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchFeuilleApi()
cfm_no = 'cfm_no_example'  # str | Resource identifier

try:
    # Retrieves a MatchFeuille resource.
    api_response = api_instance.get_match_feuille_item(cfm_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchFeuilleApi->get_match_feuille_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfm_no** | **str**| Resource identifier | 

### Return type

[**MatchFeuilleJsonldMatchFeuille**](MatchFeuilleJsonldMatchFeuille.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

