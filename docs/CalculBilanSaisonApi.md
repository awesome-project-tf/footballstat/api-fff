# swagger_client.CalculBilanSaisonApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_calcul_bilan_saison_collection**](CalculBilanSaisonApi.md#get_calcul_bilan_saison_collection) | **GET** /api/calcul_bilan_saisons | Retrieves the collection of CalculBilanSaison resources.
[**get_calcul_bilan_saison_item**](CalculBilanSaisonApi.md#get_calcul_bilan_saison_item) | **GET** /api/calcul_bilan_saisons/{id} | Retrieves a CalculBilanSaison resource.

# **get_calcul_bilan_saison_collection**
> InlineResponse2002 get_calcul_bilan_saison_collection(page=page, sa_no=sa_no, sa_no=sa_no, cl_no=cl_no, eq_no=eq_no, cp_no=cp_no, ph_no=ph_no, exists_competition=exists_competition, exists_phase=exists_phase)

Retrieves the collection of CalculBilanSaison resources.

Retrieves the collection of CalculBilanSaison resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculBilanSaisonApi()
page = 1  # int | The collection page number (optional) (default to 1)
sa_no = 56  # int |  (optional)
sa_no = [56]  # list[int] |  (optional)
cl_no = [56]  # list[int] |  (optional)
eq_no = [56]  # list[int] |  (optional)
cp_no = [56]  # list[int] |  (optional)
ph_no = [56]  # list[int] |  (optional)
exists_competition = true  # bool |  (optional)
exists_phase = true  # bool |  (optional)

try:
    # Retrieves the collection of CalculBilanSaison resources.
    api_response = api_instance.get_calcul_bilan_saison_collection(page=page, sa_no=sa_no, sa_no=sa_no, cl_no=cl_no,
                                                                   eq_no=eq_no, cp_no=cp_no, ph_no=ph_no,
                                                                   exists_competition=exists_competition,
                                                                   exists_phase=exists_phase)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculBilanSaisonApi->get_calcul_bilan_saison_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **sa_no** | **int**|  | [optional] 
 **sa_no** | [**list[int]**](int.md)|  | [optional] 
 **cl_no** | [**list[int]**](int.md)|  | [optional] 
 **eq_no** | [**list[int]**](int.md)|  | [optional] 
 **cp_no** | [**list[int]**](int.md)|  | [optional] 
 **ph_no** | [**list[int]**](int.md)|  | [optional] 
 **exists_competition** | **bool**|  | [optional] 
 **exists_phase** | **bool**|  | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calcul_bilan_saison_item**
> CalculBilanSaisonJsonldCalculBilanSaison get_calcul_bilan_saison_item(id)

Retrieves a CalculBilanSaison resource.

Retrieves a CalculBilanSaison resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculBilanSaisonApi()
id = 'id_example'  # str | Resource identifier

try:
    # Retrieves a CalculBilanSaison resource.
    api_response = api_instance.get_calcul_bilan_saison_item(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculBilanSaisonApi->get_calcul_bilan_saison_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Resource identifier | 

### Return type

[**CalculBilanSaisonJsonldCalculBilanSaison**](CalculBilanSaisonJsonldCalculBilanSaison.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

