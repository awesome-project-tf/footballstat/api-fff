# ArbitreOfficielCategorieArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ac_dat_deb** | **datetime** |  | [optional] 
**ac_dat_fin** | **datetime** |  | [optional] 
**officiel_categorie** | [**OfficielCategorieArbitre**](OfficielCategorieArbitre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

