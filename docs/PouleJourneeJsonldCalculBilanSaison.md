# PouleJourneeJsonldCalculBilanSaison

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfPouleJourneeJsonldCalculBilanSaisonContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**number** | **int** |  | [optional] 
**pj_no_tour** | **int** |  | [optional] 
**pj_lib_tour** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

