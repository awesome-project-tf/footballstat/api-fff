# IndividuJsonldOfficielCategorieArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfIndividuJsonldOfficielCategorieArbitreContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**in_nom** | **str** |  | [optional] 
**in_prenom** | **str** |  | [optional] 
**in_sexe** | **str** |  | [optional] 
**in_dat_nais** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

