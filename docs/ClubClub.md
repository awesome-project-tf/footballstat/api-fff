# ClubClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cl_no** | **int** |  | [optional] 
**district** | [**CdgClub**](CdgClub.md) |  | [optional] 
**department_code** | **int** |  | [optional] 
**affiliation_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**short_name** | **str** |  | [optional] 
**location** | **str** |  | [optional] 
**colors** | **str** |  | [optional] 
**address1** | **str** |  | [optional] 
**address2** | **str** |  | [optional] 
**address3** | **str** |  | [optional] 
**postal_code** | **str** |  | [optional] 
**distributor_office** | **str** |  | [optional] 
**latitude** | **float** |  | [optional] 
**longitude** | **float** |  | [optional] 
**terrains** | [**list[TerrainClub]**](TerrainClub.md) |  | [optional] 
**membres** | [**list[ClubTitreClub]**](ClubTitreClub.md) |  | [optional] 
**logo** | **str** |  | [optional] 
**contacts** | [**list[ContactClub]**](ContactClub.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

