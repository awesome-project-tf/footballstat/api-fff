# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_calcul_serie_cours_calcul_serie_cours_match3 import AnyOfCalculSerieCoursCalculSerieCoursMatch3  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfCalculSerieCoursCalculSerieCoursMatch3(unittest.TestCase):
    """AnyOfCalculSerieCoursCalculSerieCoursMatch3 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfCalculSerieCoursCalculSerieCoursMatch3(self):
        """Test AnyOfCalculSerieCoursCalculSerieCoursMatch3"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_calcul_serie_cours_calcul_serie_cours_match3.AnyOfCalculSerieCoursCalculSerieCoursMatch3()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
