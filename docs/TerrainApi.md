# swagger_client.TerrainApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_terrain_item**](TerrainApi.md#get_terrain_item) | **GET** /api/terrains/{teNo} | Retrieves a Terrain resource.

# **get_terrain_item**
> TerrainJsonldTerrain get_terrain_item(te_no)

Retrieves a Terrain resource.

Retrieves a Terrain resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.TerrainApi()
te_no = 'te_no_example'  # str | Resource identifier

try:
    # Retrieves a Terrain resource.
    api_response = api_instance.get_terrain_item(te_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TerrainApi->get_terrain_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **te_no** | **str**| Resource identifier | 

### Return type

[**TerrainJsonldTerrain**](TerrainJsonldTerrain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

