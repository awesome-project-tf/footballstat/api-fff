# EngagementJsonldEquipe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfEngagementJsonldEquipeContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**terrain** | **str** |  | [optional] 
**en_statut** | **str** |  | [optional] 
**en_forf_gene** | **str** |  | [optional] 
**en_tour_no** | **int** |  | [optional] 
**en_elimine** | **str** |  | [optional] 
**en_excl_class** | **str** |  | [optional] 
**competition** | [**CompetJsonldEquipe**](CompetJsonldEquipe.md) |  | [optional] 
**phase** | [**PhaseJsonldEquipe**](PhaseJsonldEquipe.md) |  | [optional] 
**poule** | [**PouleJsonldEquipe**](PouleJsonldEquipe.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

