# EquipeEquipe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**club** | **AnyOfEquipeEquipeClub** |  | [optional] 
**season** | **int** |  | [optional] 
**category_code** | **str** |  | [optional] 
**number** | **int** |  | [optional] 
**code** | **int** |  | [optional] 
**short_name** | **str** |  | [optional] 
**short_name_ligue** | **str** |  | [optional] 
**short_name_federation** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**engagements** | [**list[EngagementEquipe]**](EngagementEquipe.md) |  | [optional] 
**category_label** | **str** |  | [optional] 
**category_gender** | **str** |  | [optional] 
**diffusable** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

