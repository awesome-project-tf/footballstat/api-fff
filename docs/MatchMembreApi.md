# swagger_client.MatchMembreApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_match_membre_collection**](MatchMembreApi.md#get_match_membre_collection) | **GET** /api/match_membres | Retrieves the collection of MatchMembre resources.

# **get_match_membre_collection**
> InlineResponse20017 get_match_membre_collection(page=page, in_no=in_no, in_no=in_no, match_entity_ma_no=match_entity_ma_no, match_entity_ma_no=match_entity_ma_no, match_entity_sa_no=match_entity_sa_no, match_entity_sa_no=match_entity_sa_no, po_cod=po_cod, po_cod=po_cod, in_no_obs=in_no_obs, in_no_obs=in_no_obs, cdg_cg_no=cdg_cg_no, cdg_cg_no=cdg_cg_no, mm_no=mm_no, mm_no=mm_no, order_match_entity_ma_dat=order_match_entity_ma_dat, match_entity_ma_dat_before=match_entity_ma_dat_before, match_entity_ma_dat_strictly_before=match_entity_ma_dat_strictly_before, match_entity_ma_dat_after=match_entity_ma_dat_after, match_entity_ma_dat_strictly_after=match_entity_ma_dat_strictly_after)

Retrieves the collection of MatchMembre resources.

Retrieves the collection of MatchMembre resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.MatchMembreApi()
page = 1  # int | The collection page number (optional) (default to 1)
in_no = 56  # int |  (optional)
in_no = [56]  # list[int] |  (optional)
match_entity_ma_no = 56  # int |  (optional)
match_entity_ma_no = [56]  # list[int] |  (optional)
match_entity_sa_no = 56  # int |  (optional)
match_entity_sa_no = [56]  # list[int] |  (optional)
po_cod = 'po_cod_example'  # str |  (optional)
po_cod = ['po_cod_example']  # list[str] |  (optional)
in_no_obs = 56  # int |  (optional)
in_no_obs = [56]  # list[int] |  (optional)
cdg_cg_no = 56  # int |  (optional)
cdg_cg_no = [56]  # list[int] |  (optional)
mm_no = 56  # int |  (optional)
mm_no = [56]  # list[int] |  (optional)
order_match_entity_ma_dat = 'order_match_entity_ma_dat_example'  # str |  (optional)
match_entity_ma_dat_before = 'match_entity_ma_dat_before_example'  # str |  (optional)
match_entity_ma_dat_strictly_before = 'match_entity_ma_dat_strictly_before_example'  # str |  (optional)
match_entity_ma_dat_after = 'match_entity_ma_dat_after_example'  # str |  (optional)
match_entity_ma_dat_strictly_after = 'match_entity_ma_dat_strictly_after_example'  # str |  (optional)

try:
    # Retrieves the collection of MatchMembre resources.
    api_response = api_instance.get_match_membre_collection(page=page, in_no=in_no, in_no=in_no,
                                                            match_entity_ma_no=match_entity_ma_no,
                                                            match_entity_ma_no=match_entity_ma_no,
                                                            match_entity_sa_no=match_entity_sa_no,
                                                            match_entity_sa_no=match_entity_sa_no, po_cod=po_cod,
                                                            po_cod=po_cod, in_no_obs=in_no_obs, in_no_obs=in_no_obs,
                                                            cdg_cg_no=cdg_cg_no, cdg_cg_no=cdg_cg_no, mm_no=mm_no,
                                                            mm_no=mm_no,
                                                            order_match_entity_ma_dat=order_match_entity_ma_dat,
                                                            match_entity_ma_dat_before=match_entity_ma_dat_before,
                                                            match_entity_ma_dat_strictly_before=match_entity_ma_dat_strictly_before,
                                                            match_entity_ma_dat_after=match_entity_ma_dat_after,
                                                            match_entity_ma_dat_strictly_after=match_entity_ma_dat_strictly_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MatchMembreApi->get_match_membre_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **in_no** | **int**|  | [optional] 
 **in_no** | [**list[int]**](int.md)|  | [optional] 
 **match_entity_ma_no** | **int**|  | [optional] 
 **match_entity_ma_no** | [**list[int]**](int.md)|  | [optional] 
 **match_entity_sa_no** | **int**|  | [optional] 
 **match_entity_sa_no** | [**list[int]**](int.md)|  | [optional] 
 **po_cod** | **str**|  | [optional] 
 **po_cod** | [**list[str]**](str.md)|  | [optional] 
 **in_no_obs** | **int**|  | [optional] 
 **in_no_obs** | [**list[int]**](int.md)|  | [optional] 
 **cdg_cg_no** | **int**|  | [optional] 
 **cdg_cg_no** | [**list[int]**](int.md)|  | [optional] 
 **mm_no** | **int**|  | [optional] 
 **mm_no** | [**list[int]**](int.md)|  | [optional] 
 **order_match_entity_ma_dat** | **str**|  | [optional] 
 **match_entity_ma_dat_before** | **str**|  | [optional] 
 **match_entity_ma_dat_strictly_before** | **str**|  | [optional] 
 **match_entity_ma_dat_after** | **str**|  | [optional] 
 **match_entity_ma_dat_strictly_after** | **str**|  | [optional] 

### Return type

[**InlineResponse20017**](InlineResponse20017.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

