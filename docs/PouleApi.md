# swagger_client.PouleApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_compets_phases_poules_classement_journees_get_subresource_compet_subresource**](PouleApi.md#api_compets_phases_poules_classement_journees_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases/{phases}/poules/{poules}/classement_journees | Retrieves a Compet resource.
[**api_compets_phases_poules_get_subresource_compet_subresource**](PouleApi.md#api_compets_phases_poules_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases/{phases}/poules | Retrieves a Compet resource.
[**api_compets_phases_poules_poule_journees_get_subresource_compet_subresource**](PouleApi.md#api_compets_phases_poules_poule_journees_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases/{phases}/poules/{poules}/poule_journees | Retrieves a Compet resource.

# **api_compets_phases_poules_classement_journees_get_subresource_compet_subresource**
> InlineResponse20013 api_compets_phases_poules_classement_journees_get_subresource_compet_subresource(cp_no, phases, poules)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PouleApi()
cp_no = 'cp_no_example'  # str | Compet identifier
phases = 'phases_example'  # str | Phase identifier
poules = 'poules_example'  # str | Poule identifier

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_poules_classement_journees_get_subresource_compet_subresource(cp_no,
                                                                                                                 phases,
                                                                                                                 poules)
    pprint(api_response)
except ApiException as e:
    print(
        "Exception when calling PouleApi->api_compets_phases_poules_classement_journees_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **phases** | **str**| Phase identifier | 
 **poules** | **str**| Poule identifier | 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_compets_phases_poules_get_subresource_compet_subresource**
> InlineResponse20012 api_compets_phases_poules_get_subresource_compet_subresource(cp_no, phases, gp_no=gp_no, gp_no=gp_no)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PouleApi()
cp_no = 'cp_no_example'  # str | Compet identifier
phases = 'phases_example'  # str | Phase identifier
gp_no = 56  # int |  (optional)
gp_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_poules_get_subresource_compet_subresource(cp_no, phases, gp_no=gp_no,
                                                                                             gp_no=gp_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PouleApi->api_compets_phases_poules_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **phases** | **str**| Phase identifier | 
 **gp_no** | **int**|  | [optional] 
 **gp_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_compets_phases_poules_poule_journees_get_subresource_compet_subresource**
> InlineResponse20014 api_compets_phases_poules_poule_journees_get_subresource_compet_subresource(cp_no, phases, poules, pj_no=pj_no, pj_no=pj_no, details=details)

Retrieves a Compet resource.

Permet de récupérer les journées d'une poule. Par défaut les matchs ne sont pas retournés. Pour avoir les matchs, ajouter `pouleJourneeWithMatch` dans le champs `details[]`. Attention passer ce paramètre sans filtre sur `pj_no` peut retourner une erreur (pour cause de requête trop volumineuse)

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PouleApi()
cp_no = 'cp_no_example'  # str | Compet identifier
phases = 'phases_example'  # str | Phase identifier
poules = 'poules_example'  # str | Poule identifier
pj_no = 56  # int |  (optional)
pj_no = [56]  # list[int] |  (optional)
details = ['details_example']  # list[str] |  (optional)

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_poules_poule_journees_get_subresource_compet_subresource(cp_no,
                                                                                                            phases,
                                                                                                            poules,
                                                                                                            pj_no=pj_no,
                                                                                                            pj_no=pj_no,
                                                                                                            details=details)
    pprint(api_response)
except ApiException as e:
    print(
        "Exception when calling PouleApi->api_compets_phases_poules_poule_journees_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **phases** | **str**| Phase identifier | 
 **poules** | **str**| Poule identifier | 
 **pj_no** | **int**|  | [optional] 
 **pj_no** | [**list[int]**](int.md)|  | [optional] 
 **details** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

