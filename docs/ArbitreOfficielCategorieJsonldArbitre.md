# ArbitreOfficielCategorieJsonldArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfArbitreOfficielCategorieJsonldArbitreContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**ac_dat_deb** | **datetime** |  | [optional] 
**ac_dat_fin** | **datetime** |  | [optional] 
**officiel_categorie** | [**OfficielCategorieJsonldArbitre**](OfficielCategorieJsonldArbitre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

