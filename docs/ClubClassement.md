# ClubClassement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cl_no** | **int** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

