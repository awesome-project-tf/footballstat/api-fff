# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.cdg_jsonld_match_membre import CdgJsonldMatchMembre  # noqa: E501
from apifff.rest import ApiException


class TestCdgJsonldMatchMembre(unittest.TestCase):
    """CdgJsonldMatchMembre unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCdgJsonldMatchMembre(self):
        """Test CdgJsonldMatchMembre"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.cdg_jsonld_match_membre.CdgJsonldMatchMembre()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
