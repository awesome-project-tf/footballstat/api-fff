# MatchEntityJsonldMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchEntityJsonldMatchMembreContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**ma_no** | **int** |  | [optional] 
**competition** | **AnyOfMatchEntityJsonldMatchMembreCompetition** |  | [optional] 
**phase** | **AnyOfMatchEntityJsonldMatchMembrePhase** |  | [optional] 
**poule** | **AnyOfMatchEntityJsonldMatchMembrePoule** |  | [optional] 
**poule_journee** | **AnyOfMatchEntityJsonldMatchMembrePouleJournee** |  | [optional] 
**home** | **AnyOfMatchEntityJsonldMatchMembreHome** |  | [optional] 
**away** | **AnyOfMatchEntityJsonldMatchMembreAway** |  | [optional] 
**terrain** | **AnyOfMatchEntityJsonldMatchMembreTerrain** |  | [optional] 
**season** | **int** |  | [optional] 
**is_overtime** | **str** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**time** | **str** |  | [optional] 
**initial_date** | **datetime** |  | [optional] 
**home_resu** | **str** |  | [optional] 
**cr_nb_but** | **int** |  | [optional] 
**home_score** | **int** |  | [optional] 
**home_but_contre** | **int** |  | [optional] 
**home_nb_point** | **int** |  | [optional] 
**home_nb_tir_but** | **int** |  | [optional] 
**home_is_forfeit** | **str** |  | [optional] 
**away_resu** | **str** |  | [optional] 
**away_nb_but** | **int** |  | [optional] 
**away_score** | **int** |  | [optional] 
**away_but_contre** | **int** |  | [optional] 
**away_nb_point** | **int** |  | [optional] 
**away_nb_tir_but** | **int** |  | [optional] 
**away_nb_point_pena** | **int** |  | [optional] 
**away_is_forfeit** | **str** |  | [optional] 
**ma_fmi_pres** | **bool** |  | [optional] 
**match_membres** | [**list[MatchMembreJsonldMatchMembre]**](MatchMembreJsonldMatchMembre.md) |  | [optional] 
**status_label** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

