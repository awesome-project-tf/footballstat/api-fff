# MatchFeuilleCompoJsonldMatchFeuille

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchFeuilleCompoJsonldMatchFeuilleContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**in_no** | **str** |  | [optional] 
**usr_no** | **int** |  | [optional] 
**li_no** | **str** |  | [optional] 
**cfi_poste** | **str** |  | [optional] 
**cfi_ordre** | **int** |  | [optional] 
**cfi_maillot** | **int** |  | [optional] 
**cfi_capi** | **str** |  | [optional] 
**cfi_typ_role** | **str** |  | [optional] 
**cfi_role** | **str** |  | [optional] 
**cfi_cl_no** | **int** |  | [optional] 
**in_nom** | **str** |  | [optional] 
**in_prenom** | **str** |  | [optional] 
**li_statut** | **str** |  | [optional] 
**dat_modif** | **str** |  | [optional] 
**in_nat** | **int** |  | [optional] 
**li_mut** | **str** |  | [optional] 
**in_dat_nais** | **datetime** |  | [optional] 
**tq_no** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

