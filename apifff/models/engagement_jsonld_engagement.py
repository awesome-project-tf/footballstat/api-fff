# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class EngagementJsonldEngagement(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'type': 'str',
        'context': 'OneOfEngagementJsonldEngagementContext',
        'sa_no': 'int',
        'terrain': 'str',
        'en_statut': 'str',
        'en_forf_gene': 'str',
        'en_tour_no': 'int',
        'en_elimine': 'str',
        'en_excl_class': 'str',
        'competition': 'CompetJsonldEngagement',
        'phase': 'PhaseJsonldEngagement',
        'poule': 'PouleJsonldEngagement',
        'equipe': 'EquipeJsonldEngagement'
    }

    attribute_map = {
        'id': '@id',
        'type': '@type',
        'context': '@context',
        'sa_no': 'sa_no',
        'terrain': 'terrain',
        'en_statut': 'en_statut',
        'en_forf_gene': 'en_forf_gene',
        'en_tour_no': 'en_tour_no',
        'en_elimine': 'en_elimine',
        'en_excl_class': 'en_excl_class',
        'competition': 'competition',
        'phase': 'phase',
        'poule': 'poule',
        'equipe': 'equipe'
    }

    def __init__(self, id=None, type=None, context=None, sa_no=None, terrain=None, en_statut=None, en_forf_gene=None, en_tour_no=None, en_elimine=None, en_excl_class=None, competition=None, phase=None, poule=None, equipe=None):  # noqa: E501
        """EngagementJsonldEngagement - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._context = None
        self._sa_no = None
        self._terrain = None
        self._en_statut = None
        self._en_forf_gene = None
        self._en_tour_no = None
        self._en_elimine = None
        self._en_excl_class = None
        self._competition = None
        self._phase = None
        self._poule = None
        self._equipe = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if context is not None:
            self.context = context
        if sa_no is not None:
            self.sa_no = sa_no
        if terrain is not None:
            self.terrain = terrain
        if en_statut is not None:
            self.en_statut = en_statut
        if en_forf_gene is not None:
            self.en_forf_gene = en_forf_gene
        if en_tour_no is not None:
            self.en_tour_no = en_tour_no
        if en_elimine is not None:
            self.en_elimine = en_elimine
        if en_excl_class is not None:
            self.en_excl_class = en_excl_class
        if competition is not None:
            self.competition = competition
        if phase is not None:
            self.phase = phase
        if poule is not None:
            self.poule = poule
        if equipe is not None:
            self.equipe = equipe

    @property
    def id(self):
        """Gets the id of this EngagementJsonldEngagement.  # noqa: E501


        :return: The id of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this EngagementJsonldEngagement.


        :param id: The id of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this EngagementJsonldEngagement.  # noqa: E501


        :return: The type of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this EngagementJsonldEngagement.


        :param type: The type of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def context(self):
        """Gets the context of this EngagementJsonldEngagement.  # noqa: E501


        :return: The context of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: OneOfEngagementJsonldEngagementContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this EngagementJsonldEngagement.


        :param context: The context of this EngagementJsonldEngagement.  # noqa: E501
        :type: OneOfEngagementJsonldEngagementContext
        """

        self._context = context

    @property
    def sa_no(self):
        """Gets the sa_no of this EngagementJsonldEngagement.  # noqa: E501


        :return: The sa_no of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: int
        """
        return self._sa_no

    @sa_no.setter
    def sa_no(self, sa_no):
        """Sets the sa_no of this EngagementJsonldEngagement.


        :param sa_no: The sa_no of this EngagementJsonldEngagement.  # noqa: E501
        :type: int
        """

        self._sa_no = sa_no

    @property
    def terrain(self):
        """Gets the terrain of this EngagementJsonldEngagement.  # noqa: E501


        :return: The terrain of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._terrain

    @terrain.setter
    def terrain(self, terrain):
        """Sets the terrain of this EngagementJsonldEngagement.


        :param terrain: The terrain of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._terrain = terrain

    @property
    def en_statut(self):
        """Gets the en_statut of this EngagementJsonldEngagement.  # noqa: E501


        :return: The en_statut of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._en_statut

    @en_statut.setter
    def en_statut(self, en_statut):
        """Sets the en_statut of this EngagementJsonldEngagement.


        :param en_statut: The en_statut of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._en_statut = en_statut

    @property
    def en_forf_gene(self):
        """Gets the en_forf_gene of this EngagementJsonldEngagement.  # noqa: E501


        :return: The en_forf_gene of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._en_forf_gene

    @en_forf_gene.setter
    def en_forf_gene(self, en_forf_gene):
        """Sets the en_forf_gene of this EngagementJsonldEngagement.


        :param en_forf_gene: The en_forf_gene of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._en_forf_gene = en_forf_gene

    @property
    def en_tour_no(self):
        """Gets the en_tour_no of this EngagementJsonldEngagement.  # noqa: E501


        :return: The en_tour_no of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: int
        """
        return self._en_tour_no

    @en_tour_no.setter
    def en_tour_no(self, en_tour_no):
        """Sets the en_tour_no of this EngagementJsonldEngagement.


        :param en_tour_no: The en_tour_no of this EngagementJsonldEngagement.  # noqa: E501
        :type: int
        """

        self._en_tour_no = en_tour_no

    @property
    def en_elimine(self):
        """Gets the en_elimine of this EngagementJsonldEngagement.  # noqa: E501


        :return: The en_elimine of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._en_elimine

    @en_elimine.setter
    def en_elimine(self, en_elimine):
        """Sets the en_elimine of this EngagementJsonldEngagement.


        :param en_elimine: The en_elimine of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._en_elimine = en_elimine

    @property
    def en_excl_class(self):
        """Gets the en_excl_class of this EngagementJsonldEngagement.  # noqa: E501


        :return: The en_excl_class of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: str
        """
        return self._en_excl_class

    @en_excl_class.setter
    def en_excl_class(self, en_excl_class):
        """Sets the en_excl_class of this EngagementJsonldEngagement.


        :param en_excl_class: The en_excl_class of this EngagementJsonldEngagement.  # noqa: E501
        :type: str
        """

        self._en_excl_class = en_excl_class

    @property
    def competition(self):
        """Gets the competition of this EngagementJsonldEngagement.  # noqa: E501


        :return: The competition of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: CompetJsonldEngagement
        """
        return self._competition

    @competition.setter
    def competition(self, competition):
        """Sets the competition of this EngagementJsonldEngagement.


        :param competition: The competition of this EngagementJsonldEngagement.  # noqa: E501
        :type: CompetJsonldEngagement
        """

        self._competition = competition

    @property
    def phase(self):
        """Gets the phase of this EngagementJsonldEngagement.  # noqa: E501


        :return: The phase of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: PhaseJsonldEngagement
        """
        return self._phase

    @phase.setter
    def phase(self, phase):
        """Sets the phase of this EngagementJsonldEngagement.


        :param phase: The phase of this EngagementJsonldEngagement.  # noqa: E501
        :type: PhaseJsonldEngagement
        """

        self._phase = phase

    @property
    def poule(self):
        """Gets the poule of this EngagementJsonldEngagement.  # noqa: E501


        :return: The poule of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: PouleJsonldEngagement
        """
        return self._poule

    @poule.setter
    def poule(self, poule):
        """Sets the poule of this EngagementJsonldEngagement.


        :param poule: The poule of this EngagementJsonldEngagement.  # noqa: E501
        :type: PouleJsonldEngagement
        """

        self._poule = poule

    @property
    def equipe(self):
        """Gets the equipe of this EngagementJsonldEngagement.  # noqa: E501


        :return: The equipe of this EngagementJsonldEngagement.  # noqa: E501
        :rtype: EquipeJsonldEngagement
        """
        return self._equipe

    @equipe.setter
    def equipe(self, equipe):
        """Sets the equipe of this EngagementJsonldEngagement.


        :param equipe: The equipe of this EngagementJsonldEngagement.  # noqa: E501
        :type: EquipeJsonldEngagement
        """

        self._equipe = equipe

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(EngagementJsonldEngagement, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EngagementJsonldEngagement):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
