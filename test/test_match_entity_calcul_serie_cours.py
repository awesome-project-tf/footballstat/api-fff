# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.match_entity_calcul_serie_cours import MatchEntityCalculSerieCours  # noqa: E501
from apifff.rest import ApiException


class TestMatchEntityCalculSerieCours(unittest.TestCase):
    """MatchEntityCalculSerieCours unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testMatchEntityCalculSerieCours(self):
        """Test MatchEntityCalculSerieCours"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.match_entity_calcul_serie_cours.MatchEntityCalculSerieCours()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
