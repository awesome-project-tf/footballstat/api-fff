# CompetJsonldEngagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfCompetJsonldEngagementContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**cp_no** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**cp_tri** | **int** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgJsonldEngagement**](CdgJsonldEngagement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

