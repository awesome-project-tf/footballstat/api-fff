# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_calcul_serie_cours_jsonld_calcul_serie_cours_match1 import AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1(unittest.TestCase):
    """AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1(self):
        """Test AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_calcul_serie_cours_jsonld_calcul_serie_cours_match1.AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch1()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
