# MatchEntityJsonldDossier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchEntityJsonldDossierContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**competition** | **AnyOfMatchEntityJsonldDossierCompetition** |  | [optional] 
**home** | **AnyOfMatchEntityJsonldDossierHome** |  | [optional] 
**away** | **AnyOfMatchEntityJsonldDossierAway** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

