# MatchFeuilleJsonldMatchFeuille

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfMatchFeuilleJsonldMatchFeuilleContext** |  | [optional] 
**cfm_no** | **int** |  | [optional] 
**cfm_statut** | **int** |  | [optional] 
**ma_min_period1** | **int** |  | [optional] 
**chbx_prolongations** | **bool** |  | [optional] 
**chbx_match_arrete** | **bool** |  | [optional] 
**chbx_match_non_joue** | **bool** |  | [optional] 
**match_feuille_evenements** | [**list[MatchFeuilleEvenementJsonldMatchFeuille]**](MatchFeuilleEvenementJsonldMatchFeuille.md) |  | [optional] 
**match_feuille_compos** | [**list[MatchFeuilleCompoJsonldMatchFeuille]**](MatchFeuilleCompoJsonldMatchFeuille.md) |  | [optional] 
**match_feuille_membres** | [**list[MatchFeuilleMembreJsonldMatchFeuille]**](MatchFeuilleMembreJsonldMatchFeuille.md) |  | [optional] 
**match_feuille_reserves** | [**list[MatchFeuilleReserveJsonldMatchFeuille]**](MatchFeuilleReserveJsonldMatchFeuille.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

