# CalculClassementJsonld

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfCalculClassementJsonldContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**poule** | **str** |  | [optional] 
**cj_no** | **int** |  | [optional] 
**cj_place** | **int** |  | [optional] 
**cj_nb_but_pour** | **int** |  | [optional] 
**cj_nb_but_contre** | **int** |  | [optional] 
**cj_place_but_pour** | **int** |  | [optional] 
**cj_place_but_contre** | **int** |  | [optional] 
**date_match** | **datetime** |  | [optional] 
**equipe** | **str** |  | [optional] 
**competition** | **str** |  | [optional] 
**id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

