# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ArbitreJsonldOfficielCategorieArbitre(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'context': 'OneOfArbitreJsonldOfficielCategorieArbitreContext',
        'id': 'str',
        'type': 'str',
        'in_no': 'str',
        'li_no': 'int',
        'sa_no': 'int',
        'ar_dat1ere_lic': 'datetime',
        'ar_dat_rad': 'datetime',
        'ar_dat_integr': 'datetime',
        'ar_heur_integr': 'str',
        'individu': 'IndividuJsonldOfficielCategorieArbitre'
    }

    attribute_map = {
        'context': '@context',
        'id': '@id',
        'type': '@type',
        'in_no': 'in_no',
        'li_no': 'li_no',
        'sa_no': 'sa_no',
        'ar_dat1ere_lic': 'ar_dat1ere_lic',
        'ar_dat_rad': 'ar_dat_rad',
        'ar_dat_integr': 'ar_dat_integr',
        'ar_heur_integr': 'ar_heur_integr',
        'individu': 'individu'
    }

    def __init__(self, context=None, id=None, type=None, in_no=None, li_no=None, sa_no=None, ar_dat1ere_lic=None, ar_dat_rad=None, ar_dat_integr=None, ar_heur_integr=None, individu=None):  # noqa: E501
        """ArbitreJsonldOfficielCategorieArbitre - a model defined in Swagger"""  # noqa: E501
        self._context = None
        self._id = None
        self._type = None
        self._in_no = None
        self._li_no = None
        self._sa_no = None
        self._ar_dat1ere_lic = None
        self._ar_dat_rad = None
        self._ar_dat_integr = None
        self._ar_heur_integr = None
        self._individu = None
        self.discriminator = None
        if context is not None:
            self.context = context
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if in_no is not None:
            self.in_no = in_no
        if li_no is not None:
            self.li_no = li_no
        if sa_no is not None:
            self.sa_no = sa_no
        if ar_dat1ere_lic is not None:
            self.ar_dat1ere_lic = ar_dat1ere_lic
        if ar_dat_rad is not None:
            self.ar_dat_rad = ar_dat_rad
        if ar_dat_integr is not None:
            self.ar_dat_integr = ar_dat_integr
        if ar_heur_integr is not None:
            self.ar_heur_integr = ar_heur_integr
        if individu is not None:
            self.individu = individu

    @property
    def context(self):
        """Gets the context of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The context of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: OneOfArbitreJsonldOfficielCategorieArbitreContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this ArbitreJsonldOfficielCategorieArbitre.


        :param context: The context of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: OneOfArbitreJsonldOfficielCategorieArbitreContext
        """

        self._context = context

    @property
    def id(self):
        """Gets the id of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The id of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ArbitreJsonldOfficielCategorieArbitre.


        :param id: The id of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The type of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ArbitreJsonldOfficielCategorieArbitre.


        :param type: The type of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def in_no(self):
        """Gets the in_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The in_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: str
        """
        return self._in_no

    @in_no.setter
    def in_no(self, in_no):
        """Sets the in_no of this ArbitreJsonldOfficielCategorieArbitre.


        :param in_no: The in_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: str
        """

        self._in_no = in_no

    @property
    def li_no(self):
        """Gets the li_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The li_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: int
        """
        return self._li_no

    @li_no.setter
    def li_no(self, li_no):
        """Sets the li_no of this ArbitreJsonldOfficielCategorieArbitre.


        :param li_no: The li_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: int
        """

        self._li_no = li_no

    @property
    def sa_no(self):
        """Gets the sa_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The sa_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: int
        """
        return self._sa_no

    @sa_no.setter
    def sa_no(self, sa_no):
        """Sets the sa_no of this ArbitreJsonldOfficielCategorieArbitre.


        :param sa_no: The sa_no of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: int
        """

        self._sa_no = sa_no

    @property
    def ar_dat1ere_lic(self):
        """Gets the ar_dat1ere_lic of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The ar_dat1ere_lic of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: datetime
        """
        return self._ar_dat1ere_lic

    @ar_dat1ere_lic.setter
    def ar_dat1ere_lic(self, ar_dat1ere_lic):
        """Sets the ar_dat1ere_lic of this ArbitreJsonldOfficielCategorieArbitre.


        :param ar_dat1ere_lic: The ar_dat1ere_lic of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: datetime
        """

        self._ar_dat1ere_lic = ar_dat1ere_lic

    @property
    def ar_dat_rad(self):
        """Gets the ar_dat_rad of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The ar_dat_rad of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: datetime
        """
        return self._ar_dat_rad

    @ar_dat_rad.setter
    def ar_dat_rad(self, ar_dat_rad):
        """Sets the ar_dat_rad of this ArbitreJsonldOfficielCategorieArbitre.


        :param ar_dat_rad: The ar_dat_rad of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: datetime
        """

        self._ar_dat_rad = ar_dat_rad

    @property
    def ar_dat_integr(self):
        """Gets the ar_dat_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The ar_dat_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: datetime
        """
        return self._ar_dat_integr

    @ar_dat_integr.setter
    def ar_dat_integr(self, ar_dat_integr):
        """Sets the ar_dat_integr of this ArbitreJsonldOfficielCategorieArbitre.


        :param ar_dat_integr: The ar_dat_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: datetime
        """

        self._ar_dat_integr = ar_dat_integr

    @property
    def ar_heur_integr(self):
        """Gets the ar_heur_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The ar_heur_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: str
        """
        return self._ar_heur_integr

    @ar_heur_integr.setter
    def ar_heur_integr(self, ar_heur_integr):
        """Sets the ar_heur_integr of this ArbitreJsonldOfficielCategorieArbitre.


        :param ar_heur_integr: The ar_heur_integr of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: str
        """

        self._ar_heur_integr = ar_heur_integr

    @property
    def individu(self):
        """Gets the individu of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501


        :return: The individu of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :rtype: IndividuJsonldOfficielCategorieArbitre
        """
        return self._individu

    @individu.setter
    def individu(self, individu):
        """Sets the individu of this ArbitreJsonldOfficielCategorieArbitre.


        :param individu: The individu of this ArbitreJsonldOfficielCategorieArbitre.  # noqa: E501
        :type: IndividuJsonldOfficielCategorieArbitre
        """

        self._individu = individu

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ArbitreJsonldOfficielCategorieArbitre, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ArbitreJsonldOfficielCategorieArbitre):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
