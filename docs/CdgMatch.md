# CdgMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cg_no** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

