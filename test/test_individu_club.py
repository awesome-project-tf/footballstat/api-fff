# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.individu_club import IndividuClub  # noqa: E501
from apifff.rest import ApiException


class TestIndividuClub(unittest.TestCase):
    """IndividuClub unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testIndividuClub(self):
        """Test IndividuClub"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.individu_club.IndividuClub()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
