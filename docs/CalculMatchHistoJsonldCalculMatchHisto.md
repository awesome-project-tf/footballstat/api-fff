# CalculMatchHistoJsonldCalculMatchHisto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfCalculMatchHistoJsonldCalculMatchHistoContext** |  | [optional] 
**nb_ma_win** | **int** |  | [optional] 
**nb_ma_win_eq2** | **int** |  | [optional] 
**nb_ma_equal** | **int** |  | [optional] 
**match1** | [**MatchEntityJsonldCalculMatchHisto**](MatchEntityJsonldCalculMatchHisto.md) |  | [optional] 
**match2** | **AnyOfCalculMatchHistoJsonldCalculMatchHistoMatch2** |  | [optional] 
**match3** | **AnyOfCalculMatchHistoJsonldCalculMatchHistoMatch3** |  | [optional] 
**match4** | **AnyOfCalculMatchHistoJsonldCalculMatchHistoMatch4** |  | [optional] 
**match5** | **AnyOfCalculMatchHistoJsonldCalculMatchHistoMatch5** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

