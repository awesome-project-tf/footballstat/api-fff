# swagger_client.EquipeApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_clubs_equipes_engagements_get_subresource_club_subresource**](EquipeApi.md#api_clubs_equipes_engagements_get_subresource_club_subresource) | **GET** /api/clubs/{clNo}/equipes/{equipes}/engagements | Retrieves a Club resource.
[**api_clubs_equipes_get_subresource_club_subresource**](EquipeApi.md#api_clubs_equipes_get_subresource_club_subresource) | **GET** /api/clubs/{clNo}/equipes | Retrieves a Club resource.
[**api_clubs_equipes_matchs_get_subresource_club_subresource**](EquipeApi.md#api_clubs_equipes_matchs_get_subresource_club_subresource) | **GET** /api/clubs/{clNo}/equipes/{equipes}/matchs | Retrieves a Club resource.

# **api_clubs_equipes_engagements_get_subresource_club_subresource**
> InlineResponse2009 api_clubs_equipes_engagements_get_subresource_club_subresource(cl_no, equipes, page=page, competition_cp_no=competition_cp_no, competition_cp_no=competition_cp_no, phase_ph_no=phase_ph_no, phase_ph_no=phase_ph_no, poule_gp_no=poule_gp_no, poule_gp_no=poule_gp_no, club_cl_no=club_cl_no, club_cl_no=club_cl_no)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.EquipeApi()
cl_no = 'cl_no_example'  # str | Club identifier
equipes = 'equipes_example'  # str | Equipe identifier
page = 1  # int | The collection page number (optional) (default to 1)
competition_cp_no = 56  # int |  (optional)
competition_cp_no = [56]  # list[int] |  (optional)
phase_ph_no = 56  # int |  (optional)
phase_ph_no = [56]  # list[int] |  (optional)
poule_gp_no = 56  # int |  (optional)
poule_gp_no = [56]  # list[int] |  (optional)
club_cl_no = 56  # int |  (optional)
club_cl_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Club resource.
    api_response = api_instance.api_clubs_equipes_engagements_get_subresource_club_subresource(cl_no, equipes,
                                                                                               page=page,
                                                                                               competition_cp_no=competition_cp_no,
                                                                                               competition_cp_no=competition_cp_no,
                                                                                               phase_ph_no=phase_ph_no,
                                                                                               phase_ph_no=phase_ph_no,
                                                                                               poule_gp_no=poule_gp_no,
                                                                                               poule_gp_no=poule_gp_no,
                                                                                               club_cl_no=club_cl_no,
                                                                                               club_cl_no=club_cl_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EquipeApi->api_clubs_equipes_engagements_get_subresource_club_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Club identifier | 
 **equipes** | **str**| Equipe identifier | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **competition_cp_no** | **int**|  | [optional] 
 **competition_cp_no** | [**list[int]**](int.md)|  | [optional] 
 **phase_ph_no** | **int**|  | [optional] 
 **phase_ph_no** | [**list[int]**](int.md)|  | [optional] 
 **poule_gp_no** | **int**|  | [optional] 
 **poule_gp_no** | [**list[int]**](int.md)|  | [optional] 
 **club_cl_no** | **int**|  | [optional] 
 **club_cl_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clubs_equipes_get_subresource_club_subresource**
> InlineResponse2008 api_clubs_equipes_get_subresource_club_subresource(cl_no, page=page, eq_no=eq_no, eq_no=eq_no)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.EquipeApi()
cl_no = 'cl_no_example'  # str | Club identifier
page = 1  # int | The collection page number (optional) (default to 1)
eq_no = 56  # int |  (optional)
eq_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Club resource.
    api_response = api_instance.api_clubs_equipes_get_subresource_club_subresource(cl_no, page=page, eq_no=eq_no,
                                                                                   eq_no=eq_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EquipeApi->api_clubs_equipes_get_subresource_club_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Club identifier | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **eq_no** | **int**|  | [optional] 
 **eq_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clubs_equipes_matchs_get_subresource_club_subresource**
> InlineResponse2007 api_clubs_equipes_matchs_get_subresource_club_subresource(cl_no, equipes, page=page, cl_no=cl_no, competition_cp_no=competition_cp_no, competition_cp_no=competition_cp_no, ma_dat_before=ma_dat_before, ma_dat_strictly_before=ma_dat_strictly_before, ma_dat_after=ma_dat_after, ma_dat_strictly_after=ma_dat_strictly_after)

Retrieves a Club resource.

Retrieves a Club resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.EquipeApi()
cl_no = 'cl_no_example'  # str | Club identifier
equipes = 'equipes_example'  # str | Equipe identifier
page = 1  # int | The collection page number (optional) (default to 1)
cl_no = 56  # int |  (optional)
competition_cp_no = 56  # int |  (optional)
competition_cp_no = [56]  # list[int] |  (optional)
ma_dat_before = 'ma_dat_before_example'  # str |  (optional)
ma_dat_strictly_before = 'ma_dat_strictly_before_example'  # str |  (optional)
ma_dat_after = 'ma_dat_after_example'  # str |  (optional)
ma_dat_strictly_after = 'ma_dat_strictly_after_example'  # str |  (optional)

try:
    # Retrieves a Club resource.
    api_response = api_instance.api_clubs_equipes_matchs_get_subresource_club_subresource(cl_no, equipes, page=page,
                                                                                          cl_no=cl_no,
                                                                                          competition_cp_no=competition_cp_no,
                                                                                          competition_cp_no=competition_cp_no,
                                                                                          ma_dat_before=ma_dat_before,
                                                                                          ma_dat_strictly_before=ma_dat_strictly_before,
                                                                                          ma_dat_after=ma_dat_after,
                                                                                          ma_dat_strictly_after=ma_dat_strictly_after)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EquipeApi->api_clubs_equipes_matchs_get_subresource_club_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cl_no** | **str**| Club identifier | 
 **equipes** | **str**| Equipe identifier | 
 **page** | **int**| The collection page number | [optional] [default to 1]
 **cl_no** | **int**|  | [optional] 
 **competition_cp_no** | **int**|  | [optional] 
 **competition_cp_no** | [**list[int]**](int.md)|  | [optional] 
 **ma_dat_before** | **str**|  | [optional] 
 **ma_dat_strictly_before** | **str**|  | [optional] 
 **ma_dat_after** | **str**|  | [optional] 
 **ma_dat_strictly_after** | **str**|  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

