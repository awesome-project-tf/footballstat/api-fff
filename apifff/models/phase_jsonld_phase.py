# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PhaseJsonldPhase(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'type': 'str',
        'context': 'OneOfPhaseJsonldPhaseContext',
        'number': 'int',
        'type': 'str',
        'name': 'str',
        'has_ranking': 'str',
        'has_second_leg': 'str',
        'has_penalty': 'str',
        'has_extra_time': 'str',
        'competition': 'CompetJsonldPhase',
        'groups': 'list[PouleJsonldPhase]'
    }

    attribute_map = {
        'id': '@id',
        'type': '@type',
        'context': '@context',
        'number': 'number',
        'type': 'type',
        'name': 'name',
        'has_ranking': 'has_ranking',
        'has_second_leg': 'has_second_leg',
        'has_penalty': 'has_penalty',
        'has_extra_time': 'has_extra_time',
        'competition': 'competition',
        'groups': 'groups'
    }

    def __init__(self, id=None, type=None, context=None, number=None, name=None, has_ranking=None, has_second_leg=None, has_penalty=None, has_extra_time=None, competition=None, groups=None):  # noqa: E501
        """PhaseJsonldPhase - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._context = None
        self._number = None
        self._type = None
        self._name = None
        self._has_ranking = None
        self._has_second_leg = None
        self._has_penalty = None
        self._has_extra_time = None
        self._competition = None
        self._groups = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if context is not None:
            self.context = context
        if number is not None:
            self.number = number
        if type is not None:
            self.type = type
        if name is not None:
            self.name = name
        if has_ranking is not None:
            self.has_ranking = has_ranking
        if has_second_leg is not None:
            self.has_second_leg = has_second_leg
        if has_penalty is not None:
            self.has_penalty = has_penalty
        if has_extra_time is not None:
            self.has_extra_time = has_extra_time
        if competition is not None:
            self.competition = competition
        if groups is not None:
            self.groups = groups

    @property
    def id(self):
        """Gets the id of this PhaseJsonldPhase.  # noqa: E501


        :return: The id of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this PhaseJsonldPhase.


        :param id: The id of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this PhaseJsonldPhase.  # noqa: E501


        :return: The type of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this PhaseJsonldPhase.


        :param type: The type of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def context(self):
        """Gets the context of this PhaseJsonldPhase.  # noqa: E501


        :return: The context of this PhaseJsonldPhase.  # noqa: E501
        :rtype: OneOfPhaseJsonldPhaseContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this PhaseJsonldPhase.


        :param context: The context of this PhaseJsonldPhase.  # noqa: E501
        :type: OneOfPhaseJsonldPhaseContext
        """

        self._context = context

    @property
    def number(self):
        """Gets the number of this PhaseJsonldPhase.  # noqa: E501


        :return: The number of this PhaseJsonldPhase.  # noqa: E501
        :rtype: int
        """
        return self._number

    @number.setter
    def number(self, number):
        """Sets the number of this PhaseJsonldPhase.


        :param number: The number of this PhaseJsonldPhase.  # noqa: E501
        :type: int
        """

        self._number = number

    @property
    def type(self):
        """Gets the type of this PhaseJsonldPhase.  # noqa: E501


        :return: The type of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this PhaseJsonldPhase.


        :param type: The type of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def name(self):
        """Gets the name of this PhaseJsonldPhase.  # noqa: E501


        :return: The name of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this PhaseJsonldPhase.


        :param name: The name of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def has_ranking(self):
        """Gets the has_ranking of this PhaseJsonldPhase.  # noqa: E501


        :return: The has_ranking of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._has_ranking

    @has_ranking.setter
    def has_ranking(self, has_ranking):
        """Sets the has_ranking of this PhaseJsonldPhase.


        :param has_ranking: The has_ranking of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._has_ranking = has_ranking

    @property
    def has_second_leg(self):
        """Gets the has_second_leg of this PhaseJsonldPhase.  # noqa: E501


        :return: The has_second_leg of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._has_second_leg

    @has_second_leg.setter
    def has_second_leg(self, has_second_leg):
        """Sets the has_second_leg of this PhaseJsonldPhase.


        :param has_second_leg: The has_second_leg of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._has_second_leg = has_second_leg

    @property
    def has_penalty(self):
        """Gets the has_penalty of this PhaseJsonldPhase.  # noqa: E501


        :return: The has_penalty of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._has_penalty

    @has_penalty.setter
    def has_penalty(self, has_penalty):
        """Sets the has_penalty of this PhaseJsonldPhase.


        :param has_penalty: The has_penalty of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._has_penalty = has_penalty

    @property
    def has_extra_time(self):
        """Gets the has_extra_time of this PhaseJsonldPhase.  # noqa: E501


        :return: The has_extra_time of this PhaseJsonldPhase.  # noqa: E501
        :rtype: str
        """
        return self._has_extra_time

    @has_extra_time.setter
    def has_extra_time(self, has_extra_time):
        """Sets the has_extra_time of this PhaseJsonldPhase.


        :param has_extra_time: The has_extra_time of this PhaseJsonldPhase.  # noqa: E501
        :type: str
        """

        self._has_extra_time = has_extra_time

    @property
    def competition(self):
        """Gets the competition of this PhaseJsonldPhase.  # noqa: E501


        :return: The competition of this PhaseJsonldPhase.  # noqa: E501
        :rtype: CompetJsonldPhase
        """
        return self._competition

    @competition.setter
    def competition(self, competition):
        """Sets the competition of this PhaseJsonldPhase.


        :param competition: The competition of this PhaseJsonldPhase.  # noqa: E501
        :type: CompetJsonldPhase
        """

        self._competition = competition

    @property
    def groups(self):
        """Gets the groups of this PhaseJsonldPhase.  # noqa: E501


        :return: The groups of this PhaseJsonldPhase.  # noqa: E501
        :rtype: list[PouleJsonldPhase]
        """
        return self._groups

    @groups.setter
    def groups(self, groups):
        """Sets the groups of this PhaseJsonldPhase.


        :param groups: The groups of this PhaseJsonldPhase.  # noqa: E501
        :type: list[PouleJsonldPhase]
        """

        self._groups = groups

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PhaseJsonldPhase, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PhaseJsonldPhase):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
