# MatchMembreJsonldMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfMatchMembreJsonldMatchContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**mm_no** | **int** |  | [optional] 
**in_no** | **str** |  | [optional] 
**po_cod** | **str** |  | [optional] 
**prenom** | **str** |  | [optional] 
**nom** | **str** |  | [optional] 
**label_position** | **str** |  | [optional] 
**position_ordre** | **int** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

