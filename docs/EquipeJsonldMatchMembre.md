# EquipeJsonldMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfEquipeJsonldMatchMembreContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**club** | **AnyOfEquipeJsonldMatchMembreClub** |  | [optional] 
**short_name** | **str** |  | [optional] 
**short_name_ligue** | **str** |  | [optional] 
**short_name_federation** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

