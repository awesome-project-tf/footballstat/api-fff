# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.api.match_entity_api import MatchEntityApi  # noqa: E501
from apifff.rest import ApiException


class TestMatchEntityApi(unittest.TestCase):
    """MatchEntityApi unit test stubs"""

    def setUp(self):
        self.api = MatchEntityApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_api_clubs_equipes_matchs_get_subresource_club_subresource(self):
        """Test case for api_clubs_equipes_matchs_get_subresource_club_subresource

        Retrieves a Club resource.  # noqa: E501
        """
        pass

    def test_get_calendrier_of_club_match_entity_collection(self):
        """Test case for get_calendrier_of_club_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_calendrier_of_competition_match_entity_collection(self):
        """Test case for get_calendrier_of_competition_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_calendrier_of_equipe_match_entity_collection(self):
        """Test case for get_calendrier_of_equipe_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_match_entity_collection(self):
        """Test case for get_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_match_entity_item(self):
        """Test case for get_match_entity_item

        Retrieves a MatchEntity resource.  # noqa: E501
        """
        pass

    def test_get_matchs_of_club_match_entity_collection(self):
        """Test case for get_matchs_of_club_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_matchs_of_competition_match_entity_collection(self):
        """Test case for get_matchs_of_competition_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_resultat_of_club_match_entity_collection(self):
        """Test case for get_resultat_of_club_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_resultat_of_competition_match_entity_collection(self):
        """Test case for get_resultat_of_competition_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass

    def test_get_resultat_of_equipe_match_entity_collection(self):
        """Test case for get_resultat_of_equipe_match_entity_collection

        Retrieves the collection of MatchEntity resources.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
