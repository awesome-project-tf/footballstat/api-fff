# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.poule_jsonld_phase import PouleJsonldPhase  # noqa: E501
from apifff.rest import ApiException


class TestPouleJsonldPhase(unittest.TestCase):
    """PouleJsonldPhase unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPouleJsonldPhase(self):
        """Test PouleJsonldPhase"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.poule_jsonld_phase.PouleJsonldPhase()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
