# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_calcul_serie_cours_jsonld_calcul_serie_cours_match5 import AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5(unittest.TestCase):
    """AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5(self):
        """Test AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_calcul_serie_cours_jsonld_calcul_serie_cours_match5.AnyOfCalculSerieCoursJsonldCalculSerieCoursMatch5()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
