# EquipeCalculMatchHisto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**short_name_federation** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

