# ClassementJourneeJsonld

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfClassementJourneeJsonldContext** |  | [optional] 
**club** | **str** |  | [optional] 
**season** | **int** |  | [optional] 
**_date** | **datetime** |  | [optional] 
**cj_no** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**point_count** | **int** |  | [optional] 
**penalty_point_count** | **int** |  | [optional] 
**won_games_count** | **int** |  | [optional] 
**draw_games_count** | **int** |  | [optional] 
**lost_games_count** | **int** |  | [optional] 
**forfeits_games_count** | **int** |  | [optional] 
**goals_for_count** | **int** |  | [optional] 
**goals_against_count** | **int** |  | [optional] 
**rank** | **int** |  | [optional] 
**cj_class** | **int** |  | [optional] 
**competition** | **str** |  | [optional] 
**phase** | **str** |  | [optional] 
**poule** | **str** |  | [optional] 
**equipe** | **str** |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**id** | **int** |  | [optional] 
**goals_diff** | **int** |  | [optional] 
**total_games_count** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

