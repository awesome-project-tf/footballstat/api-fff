# ArbitreArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in_no** | **str** |  | [optional] 
**li_no** | **int** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**ar_dat1ere_lic** | **datetime** |  | [optional] 
**ar_dat_rad** | **datetime** |  | [optional] 
**ar_dat_integr** | **datetime** |  | [optional] 
**ar_heur_integr** | **str** |  | [optional] 
**individu** | **str** |  | [optional] 
**cdg** | [**CdgArbitre**](CdgArbitre.md) |  | [optional] 
**club** | **str** |  | [optional] 
**categories** | [**list[ArbitreOfficielCategorieArbitre]**](ArbitreOfficielCategorieArbitre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

