# CompetCompet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cp_no** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**license_code** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**shortname** | **str** |  | [optional] 
**cp_niv** | **str** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgCompet**](CdgCompet.md) |  | [optional] 
**phases** | [**list[PhaseCompet]**](PhaseCompet.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

