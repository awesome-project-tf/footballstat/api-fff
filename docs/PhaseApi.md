# swagger_client.PhaseApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_compets_phases_get_subresource_compet_subresource**](PhaseApi.md#api_compets_phases_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases | Retrieves a Compet resource.
[**api_compets_phases_poules_get_subresource_compet_subresource**](PhaseApi.md#api_compets_phases_poules_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases/{phases}/poules | Retrieves a Compet resource.
[**get_phase_collection**](PhaseApi.md#get_phase_collection) | **GET** /api/phases | Retrieves the collection of Phase resources.

# **api_compets_phases_get_subresource_compet_subresource**
> InlineResponse20011 api_compets_phases_get_subresource_compet_subresource(cp_no, ph_no=ph_no, ph_no=ph_no)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PhaseApi()
cp_no = 'cp_no_example'  # str | Compet identifier
ph_no = 56  # int |  (optional)
ph_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_get_subresource_compet_subresource(cp_no, ph_no=ph_no, ph_no=ph_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PhaseApi->api_compets_phases_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **ph_no** | **int**|  | [optional] 
 **ph_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_compets_phases_poules_get_subresource_compet_subresource**
> InlineResponse20012 api_compets_phases_poules_get_subresource_compet_subresource(cp_no, phases, gp_no=gp_no, gp_no=gp_no)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PhaseApi()
cp_no = 'cp_no_example'  # str | Compet identifier
phases = 'phases_example'  # str | Phase identifier
gp_no = 56  # int |  (optional)
gp_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_poules_get_subresource_compet_subresource(cp_no, phases, gp_no=gp_no,
                                                                                             gp_no=gp_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PhaseApi->api_compets_phases_poules_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **phases** | **str**| Phase identifier | 
 **gp_no** | **int**|  | [optional] 
 **gp_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_phase_collection**
> InlineResponse20011 get_phase_collection(page=page, ph_no=ph_no, ph_no=ph_no)

Retrieves the collection of Phase resources.

Retrieves the collection of Phase resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.PhaseApi()
page = 1  # int | The collection page number (optional) (default to 1)
ph_no = 56  # int |  (optional)
ph_no = [56]  # list[int] |  (optional)

try:
    # Retrieves the collection of Phase resources.
    api_response = api_instance.get_phase_collection(page=page, ph_no=ph_no, ph_no=ph_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PhaseApi->get_phase_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **ph_no** | **int**|  | [optional] 
 **ph_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

