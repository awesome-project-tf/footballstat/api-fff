# CdgMatchMembre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cg_no** | **int** |  | [optional] 
**type_label** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**max_date_referee** | **datetime** |  | [optional] 
**max_date_delegate** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

