# swagger_client.ClassementJourneeApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_compets_phases_poules_classement_journees_get_subresource_compet_subresource**](ClassementJourneeApi.md#api_compets_phases_poules_classement_journees_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases/{phases}/poules/{poules}/classement_journees | Retrieves a Compet resource.

# **api_compets_phases_poules_classement_journees_get_subresource_compet_subresource**
> InlineResponse20013 api_compets_phases_poules_classement_journees_get_subresource_compet_subresource(cp_no, phases, poules)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ClassementJourneeApi()
cp_no = 'cp_no_example'  # str | Compet identifier
phases = 'phases_example'  # str | Phase identifier
poules = 'poules_example'  # str | Poule identifier

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_poules_classement_journees_get_subresource_compet_subresource(cp_no,
                                                                                                                 phases,
                                                                                                                 poules)
    pprint(api_response)
except ApiException as e:
    print(
        "Exception when calling ClassementJourneeApi->api_compets_phases_poules_classement_journees_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **phases** | **str**| Phase identifier | 
 **poules** | **str**| Poule identifier | 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

