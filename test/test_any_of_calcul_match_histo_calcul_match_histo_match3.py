# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_calcul_match_histo_calcul_match_histo_match3 import AnyOfCalculMatchHistoCalculMatchHistoMatch3  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfCalculMatchHistoCalculMatchHistoMatch3(unittest.TestCase):
    """AnyOfCalculMatchHistoCalculMatchHistoMatch3 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfCalculMatchHistoCalculMatchHistoMatch3(self):
        """Test AnyOfCalculMatchHistoCalculMatchHistoMatch3"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_calcul_match_histo_calcul_match_histo_match3.AnyOfCalculMatchHistoCalculMatchHistoMatch3()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
