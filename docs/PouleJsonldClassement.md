# PouleJsonldClassement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfPouleJsonldClassementContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**stage_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**cdg** | [**CdgJsonldClassement**](CdgJsonldClassement.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 
**poule_unique** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

