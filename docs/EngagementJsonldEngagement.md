# EngagementJsonldEngagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfEngagementJsonldEngagementContext** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**terrain** | **str** |  | [optional] 
**en_statut** | **str** |  | [optional] 
**en_forf_gene** | **str** |  | [optional] 
**en_tour_no** | **int** |  | [optional] 
**en_elimine** | **str** |  | [optional] 
**en_excl_class** | **str** |  | [optional] 
**competition** | [**CompetJsonldEngagement**](CompetJsonldEngagement.md) |  | [optional] 
**phase** | [**PhaseJsonldEngagement**](PhaseJsonldEngagement.md) |  | [optional] 
**poule** | [**PouleJsonldEngagement**](PouleJsonldEngagement.md) |  | [optional] 
**equipe** | [**EquipeJsonldEngagement**](EquipeJsonldEngagement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

