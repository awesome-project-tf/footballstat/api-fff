# IndividuClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in_no** | **str** |  | [optional] 
**adresses** | [**list[AdresseClub]**](AdresseClub.md) |  | [optional] 
**contacts** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

