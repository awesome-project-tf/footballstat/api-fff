# MatchEntityDossier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competition** | **AnyOfMatchEntityDossierCompetition** |  | [optional] 
**home** | **AnyOfMatchEntityDossierHome** |  | [optional] 
**away** | **AnyOfMatchEntityDossierAway** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

