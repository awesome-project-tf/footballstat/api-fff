# PhaseJsonldPhase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfPhaseJsonldPhaseContext** |  | [optional] 
**number** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**has_ranking** | **str** |  | [optional] 
**has_second_leg** | **str** |  | [optional] 
**has_penalty** | **str** |  | [optional] 
**has_extra_time** | **str** |  | [optional] 
**competition** | [**CompetJsonldPhase**](CompetJsonldPhase.md) |  | [optional] 
**groups** | [**list[PouleJsonldPhase]**](PouleJsonldPhase.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

