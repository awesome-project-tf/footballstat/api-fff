# swagger_client.CompetApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_compets_phases_get_subresource_compet_subresource**](CompetApi.md#api_compets_phases_get_subresource_compet_subresource) | **GET** /api/compets/{cpNo}/phases | Retrieves a Compet resource.
[**get_compet_collection**](CompetApi.md#get_compet_collection) | **GET** /api/compets | Retrieves the collection of Compet resources.
[**get_compet_item**](CompetApi.md#get_compet_item) | **GET** /api/compets/{cpNo} | Retrieves a Compet resource.

# **api_compets_phases_get_subresource_compet_subresource**
> InlineResponse20011 api_compets_phases_get_subresource_compet_subresource(cp_no, ph_no=ph_no, ph_no=ph_no)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CompetApi()
cp_no = 'cp_no_example'  # str | Compet identifier
ph_no = 56  # int |  (optional)
ph_no = [56]  # list[int] |  (optional)

try:
    # Retrieves a Compet resource.
    api_response = api_instance.api_compets_phases_get_subresource_compet_subresource(cp_no, ph_no=ph_no, ph_no=ph_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CompetApi->api_compets_phases_get_subresource_compet_subresource: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Compet identifier | 
 **ph_no** | **int**|  | [optional] 
 **ph_no** | [**list[int]**](int.md)|  | [optional] 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_compet_collection**
> InlineResponse20010 get_compet_collection(competition_type=competition_type, cg_no=cg_no, groups=groups)

Retrieves the collection of Compet resources.

Permet de récupérer les compétitions. Par défaut on récupère toute la donnée. Pour l&d utiliser le filtre 'compet_light' dans le champs `details[]`.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CompetApi()
competition_type = 'competition_type_example'  # str |  (optional)
cg_no = 'cg_no_example'  # str |  (optional)
groups = ['groups_example']  # list[str] |  (optional)

try:
    # Retrieves the collection of Compet resources.
    api_response = api_instance.get_compet_collection(competition_type=competition_type, cg_no=cg_no, groups=groups)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CompetApi->get_compet_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **competition_type** | **str**|  | [optional] 
 **cg_no** | **str**|  | [optional] 
 **groups** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_compet_item**
> CompetJsonldCompet get_compet_item(cp_no)

Retrieves a Compet resource.

Retrieves a Compet resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CompetApi()
cp_no = 'cp_no_example'  # str | Resource identifier

try:
    # Retrieves a Compet resource.
    api_response = api_instance.get_compet_item(cp_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CompetApi->get_compet_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cp_no** | **str**| Resource identifier | 

### Return type

[**CompetJsonldCompet**](CompetJsonldCompet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

