# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.one_of_equipe_jsonld_match_context import OneOfEquipeJsonldMatchContext  # noqa: E501
from apifff.rest import ApiException


class TestOneOfEquipeJsonldMatchContext(unittest.TestCase):
    """OneOfEquipeJsonldMatchContext unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOneOfEquipeJsonldMatchContext(self):
        """Test OneOfEquipeJsonldMatchContext"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.one_of_equipe_jsonld_match_context.OneOfEquipeJsonldMatchContext()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
