# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.calcul_bilan_saison_jsonld_calcul_bilan_saison import CalculBilanSaisonJsonldCalculBilanSaison  # noqa: E501
from apifff.rest import ApiException


class TestCalculBilanSaisonJsonldCalculBilanSaison(unittest.TestCase):
    """CalculBilanSaisonJsonldCalculBilanSaison unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCalculBilanSaisonJsonldCalculBilanSaison(self):
        """Test CalculBilanSaisonJsonldCalculBilanSaison"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.calcul_bilan_saison_jsonld_calcul_bilan_saison.CalculBilanSaisonJsonldCalculBilanSaison()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
