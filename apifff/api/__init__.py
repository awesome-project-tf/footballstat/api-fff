from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from apifff.api.arbitre_api import ArbitreApi
from apifff.api.arbitre_officiel_categorie_api import ArbitreOfficielCategorieApi
from apifff.api.calcul_bilan_saison_api import CalculBilanSaisonApi
from apifff.api.calcul_match_histo_api import CalculMatchHistoApi
from apifff.api.calcul_serie_cours_api import CalculSerieCoursApi
from apifff.api.cdg_api import CdgApi
from apifff.api.classement_journee_api import ClassementJourneeApi
from apifff.api.club_api import ClubApi
from apifff.api.compet_api import CompetApi
from apifff.api.dossier_api import DossierApi
from apifff.api.engagement_api import EngagementApi
from apifff.api.equipe_api import EquipeApi
from apifff.api.individu_api import IndividuApi
from apifff.api.match_entity_api import MatchEntityApi
from apifff.api.match_feuille_api import MatchFeuilleApi
from apifff.api.match_membre_api import MatchMembreApi
from apifff.api.officiel_categorie_api import OfficielCategorieApi
from apifff.api.phase_api import PhaseApi
from apifff.api.poule_api import PouleApi
from apifff.api.poule_journee_api import PouleJourneeApi
from apifff.api.terrain_api import TerrainApi
