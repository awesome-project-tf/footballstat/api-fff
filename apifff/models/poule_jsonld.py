# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PouleJsonld(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'type': 'str',
        'stage_number': 'int',
        'season': 'int',
        'name': 'str',
        'code': 'str',
        'status': 'str',
        'gp_diff_info': 'str',
        'gp_diff_no_tour': 'int',
        'phase': 'str',
        'competition': 'str',
        'cdg': 'str',
        'rounds': 'list[str]',
        'classement_journees': 'list[str]',
        'calcul_classement': 'list[CalculClassementJsonld]',
        'external_updated_at': 'datetime',
        'id': 'int',
        'external_message_id': 'str',
        'calcul_classements': 'list[CalculClassementJsonld]',
        'poule_unique': 'bool',
        'diffusable': 'bool',
        'matchs': 'bool',
        'at_least_one_match_resultat': 'bool'
    }

    attribute_map = {
        'id': '@id',
        'type': '@type',
        'stage_number': 'stage_number',
        'season': 'season',
        'name': 'name',
        'code': 'code',
        'status': 'status',
        'gp_diff_info': 'gp_diff_info',
        'gp_diff_no_tour': 'gp_diff_no_tour',
        'phase': 'phase',
        'competition': 'competition',
        'cdg': 'cdg',
        'rounds': 'rounds',
        'classement_journees': 'classement_journees',
        'calcul_classement': 'calcul_classement',
        'external_updated_at': 'external_updated_at',
        'id': 'id',
        'external_message_id': 'external_message_id',
        'calcul_classements': 'calcul_classements',
        'poule_unique': 'poule_unique',
        'diffusable': 'diffusable',
        'matchs': 'matchs',
        'at_least_one_match_resultat': 'at_least_one_match_resultat'
    }

    def __init__(self, id=None, type=None, stage_number=None, season=None, name=None, code=None, status=None, gp_diff_info=None, gp_diff_no_tour=None, phase=None, competition=None, cdg=None, rounds=None, classement_journees=None, calcul_classement=None, external_updated_at=None, external_message_id=None, calcul_classements=None, poule_unique=None, diffusable=None, matchs=None, at_least_one_match_resultat=None):  # noqa: E501
        """PouleJsonld - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._stage_number = None
        self._season = None
        self._name = None
        self._code = None
        self._status = None
        self._gp_diff_info = None
        self._gp_diff_no_tour = None
        self._phase = None
        self._competition = None
        self._cdg = None
        self._rounds = None
        self._classement_journees = None
        self._calcul_classement = None
        self._external_updated_at = None
        self._id = None
        self._external_message_id = None
        self._calcul_classements = None
        self._poule_unique = None
        self._diffusable = None
        self._matchs = None
        self._at_least_one_match_resultat = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if stage_number is not None:
            self.stage_number = stage_number
        if season is not None:
            self.season = season
        if name is not None:
            self.name = name
        if code is not None:
            self.code = code
        if status is not None:
            self.status = status
        if gp_diff_info is not None:
            self.gp_diff_info = gp_diff_info
        if gp_diff_no_tour is not None:
            self.gp_diff_no_tour = gp_diff_no_tour
        if phase is not None:
            self.phase = phase
        if competition is not None:
            self.competition = competition
        if cdg is not None:
            self.cdg = cdg
        if rounds is not None:
            self.rounds = rounds
        if classement_journees is not None:
            self.classement_journees = classement_journees
        if calcul_classement is not None:
            self.calcul_classement = calcul_classement
        if external_updated_at is not None:
            self.external_updated_at = external_updated_at
        if id is not None:
            self.id = id
        if external_message_id is not None:
            self.external_message_id = external_message_id
        if calcul_classements is not None:
            self.calcul_classements = calcul_classements
        if poule_unique is not None:
            self.poule_unique = poule_unique
        if diffusable is not None:
            self.diffusable = diffusable
        if matchs is not None:
            self.matchs = matchs
        if at_least_one_match_resultat is not None:
            self.at_least_one_match_resultat = at_least_one_match_resultat

    @property
    def id(self):
        """Gets the id of this PouleJsonld.  # noqa: E501


        :return: The id of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this PouleJsonld.


        :param id: The id of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this PouleJsonld.  # noqa: E501


        :return: The type of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this PouleJsonld.


        :param type: The type of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def stage_number(self):
        """Gets the stage_number of this PouleJsonld.  # noqa: E501


        :return: The stage_number of this PouleJsonld.  # noqa: E501
        :rtype: int
        """
        return self._stage_number

    @stage_number.setter
    def stage_number(self, stage_number):
        """Sets the stage_number of this PouleJsonld.


        :param stage_number: The stage_number of this PouleJsonld.  # noqa: E501
        :type: int
        """

        self._stage_number = stage_number

    @property
    def season(self):
        """Gets the season of this PouleJsonld.  # noqa: E501


        :return: The season of this PouleJsonld.  # noqa: E501
        :rtype: int
        """
        return self._season

    @season.setter
    def season(self, season):
        """Sets the season of this PouleJsonld.


        :param season: The season of this PouleJsonld.  # noqa: E501
        :type: int
        """

        self._season = season

    @property
    def name(self):
        """Gets the name of this PouleJsonld.  # noqa: E501


        :return: The name of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this PouleJsonld.


        :param name: The name of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def code(self):
        """Gets the code of this PouleJsonld.  # noqa: E501


        :return: The code of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this PouleJsonld.


        :param code: The code of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._code = code

    @property
    def status(self):
        """Gets the status of this PouleJsonld.  # noqa: E501


        :return: The status of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this PouleJsonld.


        :param status: The status of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._status = status

    @property
    def gp_diff_info(self):
        """Gets the gp_diff_info of this PouleJsonld.  # noqa: E501


        :return: The gp_diff_info of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._gp_diff_info

    @gp_diff_info.setter
    def gp_diff_info(self, gp_diff_info):
        """Sets the gp_diff_info of this PouleJsonld.


        :param gp_diff_info: The gp_diff_info of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._gp_diff_info = gp_diff_info

    @property
    def gp_diff_no_tour(self):
        """Gets the gp_diff_no_tour of this PouleJsonld.  # noqa: E501


        :return: The gp_diff_no_tour of this PouleJsonld.  # noqa: E501
        :rtype: int
        """
        return self._gp_diff_no_tour

    @gp_diff_no_tour.setter
    def gp_diff_no_tour(self, gp_diff_no_tour):
        """Sets the gp_diff_no_tour of this PouleJsonld.


        :param gp_diff_no_tour: The gp_diff_no_tour of this PouleJsonld.  # noqa: E501
        :type: int
        """

        self._gp_diff_no_tour = gp_diff_no_tour

    @property
    def phase(self):
        """Gets the phase of this PouleJsonld.  # noqa: E501


        :return: The phase of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._phase

    @phase.setter
    def phase(self, phase):
        """Sets the phase of this PouleJsonld.


        :param phase: The phase of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._phase = phase

    @property
    def competition(self):
        """Gets the competition of this PouleJsonld.  # noqa: E501


        :return: The competition of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._competition

    @competition.setter
    def competition(self, competition):
        """Sets the competition of this PouleJsonld.


        :param competition: The competition of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._competition = competition

    @property
    def cdg(self):
        """Gets the cdg of this PouleJsonld.  # noqa: E501


        :return: The cdg of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._cdg

    @cdg.setter
    def cdg(self, cdg):
        """Sets the cdg of this PouleJsonld.


        :param cdg: The cdg of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._cdg = cdg

    @property
    def rounds(self):
        """Gets the rounds of this PouleJsonld.  # noqa: E501


        :return: The rounds of this PouleJsonld.  # noqa: E501
        :rtype: list[str]
        """
        return self._rounds

    @rounds.setter
    def rounds(self, rounds):
        """Sets the rounds of this PouleJsonld.


        :param rounds: The rounds of this PouleJsonld.  # noqa: E501
        :type: list[str]
        """

        self._rounds = rounds

    @property
    def classement_journees(self):
        """Gets the classement_journees of this PouleJsonld.  # noqa: E501


        :return: The classement_journees of this PouleJsonld.  # noqa: E501
        :rtype: list[str]
        """
        return self._classement_journees

    @classement_journees.setter
    def classement_journees(self, classement_journees):
        """Sets the classement_journees of this PouleJsonld.


        :param classement_journees: The classement_journees of this PouleJsonld.  # noqa: E501
        :type: list[str]
        """

        self._classement_journees = classement_journees

    @property
    def calcul_classement(self):
        """Gets the calcul_classement of this PouleJsonld.  # noqa: E501


        :return: The calcul_classement of this PouleJsonld.  # noqa: E501
        :rtype: list[CalculClassementJsonld]
        """
        return self._calcul_classement

    @calcul_classement.setter
    def calcul_classement(self, calcul_classement):
        """Sets the calcul_classement of this PouleJsonld.


        :param calcul_classement: The calcul_classement of this PouleJsonld.  # noqa: E501
        :type: list[CalculClassementJsonld]
        """

        self._calcul_classement = calcul_classement

    @property
    def external_updated_at(self):
        """Gets the external_updated_at of this PouleJsonld.  # noqa: E501


        :return: The external_updated_at of this PouleJsonld.  # noqa: E501
        :rtype: datetime
        """
        return self._external_updated_at

    @external_updated_at.setter
    def external_updated_at(self, external_updated_at):
        """Sets the external_updated_at of this PouleJsonld.


        :param external_updated_at: The external_updated_at of this PouleJsonld.  # noqa: E501
        :type: datetime
        """

        self._external_updated_at = external_updated_at

    @property
    def id(self):
        """Gets the id of this PouleJsonld.  # noqa: E501


        :return: The id of this PouleJsonld.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this PouleJsonld.


        :param id: The id of this PouleJsonld.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def external_message_id(self):
        """Gets the external_message_id of this PouleJsonld.  # noqa: E501


        :return: The external_message_id of this PouleJsonld.  # noqa: E501
        :rtype: str
        """
        return self._external_message_id

    @external_message_id.setter
    def external_message_id(self, external_message_id):
        """Sets the external_message_id of this PouleJsonld.


        :param external_message_id: The external_message_id of this PouleJsonld.  # noqa: E501
        :type: str
        """

        self._external_message_id = external_message_id

    @property
    def calcul_classements(self):
        """Gets the calcul_classements of this PouleJsonld.  # noqa: E501


        :return: The calcul_classements of this PouleJsonld.  # noqa: E501
        :rtype: list[CalculClassementJsonld]
        """
        return self._calcul_classements

    @calcul_classements.setter
    def calcul_classements(self, calcul_classements):
        """Sets the calcul_classements of this PouleJsonld.


        :param calcul_classements: The calcul_classements of this PouleJsonld.  # noqa: E501
        :type: list[CalculClassementJsonld]
        """

        self._calcul_classements = calcul_classements

    @property
    def poule_unique(self):
        """Gets the poule_unique of this PouleJsonld.  # noqa: E501


        :return: The poule_unique of this PouleJsonld.  # noqa: E501
        :rtype: bool
        """
        return self._poule_unique

    @poule_unique.setter
    def poule_unique(self, poule_unique):
        """Sets the poule_unique of this PouleJsonld.


        :param poule_unique: The poule_unique of this PouleJsonld.  # noqa: E501
        :type: bool
        """

        self._poule_unique = poule_unique

    @property
    def diffusable(self):
        """Gets the diffusable of this PouleJsonld.  # noqa: E501


        :return: The diffusable of this PouleJsonld.  # noqa: E501
        :rtype: bool
        """
        return self._diffusable

    @diffusable.setter
    def diffusable(self, diffusable):
        """Sets the diffusable of this PouleJsonld.


        :param diffusable: The diffusable of this PouleJsonld.  # noqa: E501
        :type: bool
        """

        self._diffusable = diffusable

    @property
    def matchs(self):
        """Gets the matchs of this PouleJsonld.  # noqa: E501


        :return: The matchs of this PouleJsonld.  # noqa: E501
        :rtype: bool
        """
        return self._matchs

    @matchs.setter
    def matchs(self, matchs):
        """Sets the matchs of this PouleJsonld.


        :param matchs: The matchs of this PouleJsonld.  # noqa: E501
        :type: bool
        """

        self._matchs = matchs

    @property
    def at_least_one_match_resultat(self):
        """Gets the at_least_one_match_resultat of this PouleJsonld.  # noqa: E501


        :return: The at_least_one_match_resultat of this PouleJsonld.  # noqa: E501
        :rtype: bool
        """
        return self._at_least_one_match_resultat

    @at_least_one_match_resultat.setter
    def at_least_one_match_resultat(self, at_least_one_match_resultat):
        """Sets the at_least_one_match_resultat of this PouleJsonld.


        :param at_least_one_match_resultat: The at_least_one_match_resultat of this PouleJsonld.  # noqa: E501
        :type: bool
        """

        self._at_least_one_match_resultat = at_least_one_match_resultat

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PouleJsonld, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PouleJsonld):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
