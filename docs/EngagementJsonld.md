# EngagementJsonld

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**club** | **str** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**terrain** | **str** |  | [optional] 
**en_statut** | **str** |  | [optional] 
**en_forf_gene** | **str** |  | [optional] 
**en_tour_no** | **int** |  | [optional] 
**en_elimine** | **str** |  | [optional] 
**en_excl_class** | **str** |  | [optional] 
**competition** | **str** |  | [optional] 
**phase** | **str** |  | [optional] 
**poule** | **str** |  | [optional] 
**equipe** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**external_message_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

