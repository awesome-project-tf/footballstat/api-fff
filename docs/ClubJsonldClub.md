# ClubJsonldClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfClubJsonldClubContext** |  | [optional] 
**cl_no** | **int** |  | [optional] 
**district** | [**CdgJsonldClub**](CdgJsonldClub.md) |  | [optional] 
**department_code** | **int** |  | [optional] 
**affiliation_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**short_name** | **str** |  | [optional] 
**location** | **str** |  | [optional] 
**colors** | **str** |  | [optional] 
**address1** | **str** |  | [optional] 
**address2** | **str** |  | [optional] 
**address3** | **str** |  | [optional] 
**postal_code** | **str** |  | [optional] 
**distributor_office** | **str** |  | [optional] 
**latitude** | **float** |  | [optional] 
**longitude** | **float** |  | [optional] 
**terrains** | [**list[TerrainJsonldClub]**](TerrainJsonldClub.md) |  | [optional] 
**membres** | [**list[ClubTitreJsonldClub]**](ClubTitreJsonldClub.md) |  | [optional] 
**logo** | **str** |  | [optional] 
**contacts** | [**list[ContactJsonldClub]**](ContactJsonldClub.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

