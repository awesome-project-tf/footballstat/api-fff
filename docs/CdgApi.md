# swagger_client.CdgApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_cdg_collection**](CdgApi.md#get_cdg_collection) | **GET** /api/cdgs | Retrieves the collection of Cdg resources.
[**get_cdg_item**](CdgApi.md#get_cdg_item) | **GET** /api/cdgs/{cgNo} | Retrieves a Cdg resource.

# **get_cdg_collection**
> InlineResponse2005 get_cdg_collection(cg_typ=cg_typ, cg_typ=cg_typ, cg_cp=cg_cp, cg_cp=cg_cp, cg_no_supp=cg_no_supp, cg_no_supp=cg_no_supp, cp_cod=cp_cod)

Retrieves the collection of Cdg resources.

Retrieves the collection of Cdg resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CdgApi()
cg_typ = 'cg_typ_example'  # str |  (optional)
cg_typ = ['cg_typ_example']  # list[str] |  (optional)
cg_cp = 'cg_cp_example'  # str |  (optional)
cg_cp = ['cg_cp_example']  # list[str] |  (optional)
cg_no_supp = 56  # int |  (optional)
cg_no_supp = [56]  # list[int] |  (optional)
cp_cod = 'cp_cod_example'  # str |  (optional)

try:
    # Retrieves the collection of Cdg resources.
    api_response = api_instance.get_cdg_collection(cg_typ=cg_typ, cg_typ=cg_typ, cg_cp=cg_cp, cg_cp=cg_cp,
                                                   cg_no_supp=cg_no_supp, cg_no_supp=cg_no_supp, cp_cod=cp_cod)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CdgApi->get_cdg_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cg_typ** | **str**|  | [optional] 
 **cg_typ** | [**list[str]**](str.md)|  | [optional] 
 **cg_cp** | **str**|  | [optional] 
 **cg_cp** | [**list[str]**](str.md)|  | [optional] 
 **cg_no_supp** | **int**|  | [optional] 
 **cg_no_supp** | [**list[int]**](int.md)|  | [optional] 
 **cp_cod** | **str**|  | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_cdg_item**
> CdgJsonldCdg get_cdg_item(cg_no)

Retrieves a Cdg resource.

Retrieves a Cdg resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CdgApi()
cg_no = 'cg_no_example'  # str | Resource identifier

try:
    # Retrieves a Cdg resource.
    api_response = api_instance.get_cdg_item(cg_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CdgApi->get_cdg_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cg_no** | **str**| Resource identifier | 

### Return type

[**CdgJsonldCdg**](CdgJsonldCdg.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

