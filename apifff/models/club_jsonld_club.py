# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ClubJsonldClub(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'type': 'str',
        'context': 'OneOfClubJsonldClubContext',
        'cl_no': 'int',
        'district': 'CdgJsonldClub',
        'department_code': 'int',
        'affiliation_number': 'int',
        'name': 'str',
        'short_name': 'str',
        'location': 'str',
        'colors': 'str',
        'address1': 'str',
        'address2': 'str',
        'address3': 'str',
        'postal_code': 'str',
        'distributor_office': 'str',
        'latitude': 'float',
        'longitude': 'float',
        'terrains': 'list[TerrainJsonldClub]',
        'membres': 'list[ClubTitreJsonldClub]',
        'logo': 'str',
        'contacts': 'list[ContactJsonldClub]'
    }

    attribute_map = {
        'id': '@id',
        'type': '@type',
        'context': '@context',
        'cl_no': 'cl_no',
        'district': 'district',
        'department_code': 'department_code',
        'affiliation_number': 'affiliation_number',
        'name': 'name',
        'short_name': 'short_name',
        'location': 'location',
        'colors': 'colors',
        'address1': 'address1',
        'address2': 'address2',
        'address3': 'address3',
        'postal_code': 'postal_code',
        'distributor_office': 'distributor_office',
        'latitude': 'latitude',
        'longitude': 'longitude',
        'terrains': 'terrains',
        'membres': 'membres',
        'logo': 'logo',
        'contacts': 'contacts'
    }

    def __init__(self, id=None, type=None, context=None, cl_no=None, district=None, department_code=None, affiliation_number=None, name=None, short_name=None, location=None, colors=None, address1=None, address2=None, address3=None, postal_code=None, distributor_office=None, latitude=None, longitude=None, terrains=None, membres=None, logo=None, contacts=None):  # noqa: E501
        """ClubJsonldClub - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._context = None
        self._cl_no = None
        self._district = None
        self._department_code = None
        self._affiliation_number = None
        self._name = None
        self._short_name = None
        self._location = None
        self._colors = None
        self._address1 = None
        self._address2 = None
        self._address3 = None
        self._postal_code = None
        self._distributor_office = None
        self._latitude = None
        self._longitude = None
        self._terrains = None
        self._membres = None
        self._logo = None
        self._contacts = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if context is not None:
            self.context = context
        if cl_no is not None:
            self.cl_no = cl_no
        if district is not None:
            self.district = district
        if department_code is not None:
            self.department_code = department_code
        if affiliation_number is not None:
            self.affiliation_number = affiliation_number
        if name is not None:
            self.name = name
        if short_name is not None:
            self.short_name = short_name
        if location is not None:
            self.location = location
        if colors is not None:
            self.colors = colors
        if address1 is not None:
            self.address1 = address1
        if address2 is not None:
            self.address2 = address2
        if address3 is not None:
            self.address3 = address3
        if postal_code is not None:
            self.postal_code = postal_code
        if distributor_office is not None:
            self.distributor_office = distributor_office
        if latitude is not None:
            self.latitude = latitude
        if longitude is not None:
            self.longitude = longitude
        if terrains is not None:
            self.terrains = terrains
        if membres is not None:
            self.membres = membres
        if logo is not None:
            self.logo = logo
        if contacts is not None:
            self.contacts = contacts

    @property
    def id(self):
        """Gets the id of this ClubJsonldClub.  # noqa: E501


        :return: The id of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ClubJsonldClub.


        :param id: The id of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this ClubJsonldClub.  # noqa: E501


        :return: The type of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ClubJsonldClub.


        :param type: The type of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def context(self):
        """Gets the context of this ClubJsonldClub.  # noqa: E501


        :return: The context of this ClubJsonldClub.  # noqa: E501
        :rtype: OneOfClubJsonldClubContext
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this ClubJsonldClub.


        :param context: The context of this ClubJsonldClub.  # noqa: E501
        :type: OneOfClubJsonldClubContext
        """

        self._context = context

    @property
    def cl_no(self):
        """Gets the cl_no of this ClubJsonldClub.  # noqa: E501


        :return: The cl_no of this ClubJsonldClub.  # noqa: E501
        :rtype: int
        """
        return self._cl_no

    @cl_no.setter
    def cl_no(self, cl_no):
        """Sets the cl_no of this ClubJsonldClub.


        :param cl_no: The cl_no of this ClubJsonldClub.  # noqa: E501
        :type: int
        """

        self._cl_no = cl_no

    @property
    def district(self):
        """Gets the district of this ClubJsonldClub.  # noqa: E501


        :return: The district of this ClubJsonldClub.  # noqa: E501
        :rtype: CdgJsonldClub
        """
        return self._district

    @district.setter
    def district(self, district):
        """Sets the district of this ClubJsonldClub.


        :param district: The district of this ClubJsonldClub.  # noqa: E501
        :type: CdgJsonldClub
        """

        self._district = district

    @property
    def department_code(self):
        """Gets the department_code of this ClubJsonldClub.  # noqa: E501


        :return: The department_code of this ClubJsonldClub.  # noqa: E501
        :rtype: int
        """
        return self._department_code

    @department_code.setter
    def department_code(self, department_code):
        """Sets the department_code of this ClubJsonldClub.


        :param department_code: The department_code of this ClubJsonldClub.  # noqa: E501
        :type: int
        """

        self._department_code = department_code

    @property
    def affiliation_number(self):
        """Gets the affiliation_number of this ClubJsonldClub.  # noqa: E501


        :return: The affiliation_number of this ClubJsonldClub.  # noqa: E501
        :rtype: int
        """
        return self._affiliation_number

    @affiliation_number.setter
    def affiliation_number(self, affiliation_number):
        """Sets the affiliation_number of this ClubJsonldClub.


        :param affiliation_number: The affiliation_number of this ClubJsonldClub.  # noqa: E501
        :type: int
        """

        self._affiliation_number = affiliation_number

    @property
    def name(self):
        """Gets the name of this ClubJsonldClub.  # noqa: E501


        :return: The name of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ClubJsonldClub.


        :param name: The name of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def short_name(self):
        """Gets the short_name of this ClubJsonldClub.  # noqa: E501


        :return: The short_name of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._short_name

    @short_name.setter
    def short_name(self, short_name):
        """Sets the short_name of this ClubJsonldClub.


        :param short_name: The short_name of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._short_name = short_name

    @property
    def location(self):
        """Gets the location of this ClubJsonldClub.  # noqa: E501


        :return: The location of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._location

    @location.setter
    def location(self, location):
        """Sets the location of this ClubJsonldClub.


        :param location: The location of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._location = location

    @property
    def colors(self):
        """Gets the colors of this ClubJsonldClub.  # noqa: E501


        :return: The colors of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._colors

    @colors.setter
    def colors(self, colors):
        """Sets the colors of this ClubJsonldClub.


        :param colors: The colors of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._colors = colors

    @property
    def address1(self):
        """Gets the address1 of this ClubJsonldClub.  # noqa: E501


        :return: The address1 of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._address1

    @address1.setter
    def address1(self, address1):
        """Sets the address1 of this ClubJsonldClub.


        :param address1: The address1 of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._address1 = address1

    @property
    def address2(self):
        """Gets the address2 of this ClubJsonldClub.  # noqa: E501


        :return: The address2 of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._address2

    @address2.setter
    def address2(self, address2):
        """Sets the address2 of this ClubJsonldClub.


        :param address2: The address2 of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._address2 = address2

    @property
    def address3(self):
        """Gets the address3 of this ClubJsonldClub.  # noqa: E501


        :return: The address3 of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._address3

    @address3.setter
    def address3(self, address3):
        """Sets the address3 of this ClubJsonldClub.


        :param address3: The address3 of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._address3 = address3

    @property
    def postal_code(self):
        """Gets the postal_code of this ClubJsonldClub.  # noqa: E501


        :return: The postal_code of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._postal_code

    @postal_code.setter
    def postal_code(self, postal_code):
        """Sets the postal_code of this ClubJsonldClub.


        :param postal_code: The postal_code of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._postal_code = postal_code

    @property
    def distributor_office(self):
        """Gets the distributor_office of this ClubJsonldClub.  # noqa: E501


        :return: The distributor_office of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._distributor_office

    @distributor_office.setter
    def distributor_office(self, distributor_office):
        """Sets the distributor_office of this ClubJsonldClub.


        :param distributor_office: The distributor_office of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._distributor_office = distributor_office

    @property
    def latitude(self):
        """Gets the latitude of this ClubJsonldClub.  # noqa: E501


        :return: The latitude of this ClubJsonldClub.  # noqa: E501
        :rtype: float
        """
        return self._latitude

    @latitude.setter
    def latitude(self, latitude):
        """Sets the latitude of this ClubJsonldClub.


        :param latitude: The latitude of this ClubJsonldClub.  # noqa: E501
        :type: float
        """

        self._latitude = latitude

    @property
    def longitude(self):
        """Gets the longitude of this ClubJsonldClub.  # noqa: E501


        :return: The longitude of this ClubJsonldClub.  # noqa: E501
        :rtype: float
        """
        return self._longitude

    @longitude.setter
    def longitude(self, longitude):
        """Sets the longitude of this ClubJsonldClub.


        :param longitude: The longitude of this ClubJsonldClub.  # noqa: E501
        :type: float
        """

        self._longitude = longitude

    @property
    def terrains(self):
        """Gets the terrains of this ClubJsonldClub.  # noqa: E501


        :return: The terrains of this ClubJsonldClub.  # noqa: E501
        :rtype: list[TerrainJsonldClub]
        """
        return self._terrains

    @terrains.setter
    def terrains(self, terrains):
        """Sets the terrains of this ClubJsonldClub.


        :param terrains: The terrains of this ClubJsonldClub.  # noqa: E501
        :type: list[TerrainJsonldClub]
        """

        self._terrains = terrains

    @property
    def membres(self):
        """Gets the membres of this ClubJsonldClub.  # noqa: E501


        :return: The membres of this ClubJsonldClub.  # noqa: E501
        :rtype: list[ClubTitreJsonldClub]
        """
        return self._membres

    @membres.setter
    def membres(self, membres):
        """Sets the membres of this ClubJsonldClub.


        :param membres: The membres of this ClubJsonldClub.  # noqa: E501
        :type: list[ClubTitreJsonldClub]
        """

        self._membres = membres

    @property
    def logo(self):
        """Gets the logo of this ClubJsonldClub.  # noqa: E501


        :return: The logo of this ClubJsonldClub.  # noqa: E501
        :rtype: str
        """
        return self._logo

    @logo.setter
    def logo(self, logo):
        """Sets the logo of this ClubJsonldClub.


        :param logo: The logo of this ClubJsonldClub.  # noqa: E501
        :type: str
        """

        self._logo = logo

    @property
    def contacts(self):
        """Gets the contacts of this ClubJsonldClub.  # noqa: E501


        :return: The contacts of this ClubJsonldClub.  # noqa: E501
        :rtype: list[ContactJsonldClub]
        """
        return self._contacts

    @contacts.setter
    def contacts(self, contacts):
        """Sets the contacts of this ClubJsonldClub.


        :param contacts: The contacts of this ClubJsonldClub.  # noqa: E501
        :type: list[ContactJsonldClub]
        """

        self._contacts = contacts

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ClubJsonldClub, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ClubJsonldClub):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
