# CompetJsonldMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfCompetJsonldMatchContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**cp_no** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**level** | **str** |  | [optional] 
**cdg** | [**CdgJsonldMatch**](CdgJsonldMatch.md) |  | [optional] 
**external_updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

