# MatchFeuilleMembreMatchFeuille

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mmf_no** | **int** |  | [optional] 
**mm_no** | **int** |  | [optional] 
**po_cod** | **str** |  | [optional] 
**po_lib** | **str** |  | [optional] 
**mmf_typ** | **str** |  | [optional] 
**mmf_statut** | **str** |  | [optional] 
**mmf_pres** | **str** |  | [optional] 
**mmf_in_nom** | **str** |  | [optional] 
**mmf_in_prenom** | **str** |  | [optional] 
**mmf_typ_piece** | **str** |  | [optional] 
**mmf_in_no** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

