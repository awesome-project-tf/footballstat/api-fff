# ArbitreJsonldArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**context** | **OneOfArbitreJsonldArbitreContext** |  | [optional] 
**in_no** | **str** |  | [optional] 
**li_no** | **int** |  | [optional] 
**sa_no** | **int** |  | [optional] 
**ar_dat1ere_lic** | **datetime** |  | [optional] 
**ar_dat_rad** | **datetime** |  | [optional] 
**ar_dat_integr** | **datetime** |  | [optional] 
**ar_heur_integr** | **str** |  | [optional] 
**individu** | **str** |  | [optional] 
**cdg** | [**CdgJsonldArbitre**](CdgJsonldArbitre.md) |  | [optional] 
**club** | **str** |  | [optional] 
**categories** | [**list[ArbitreOfficielCategorieJsonldArbitre]**](ArbitreOfficielCategorieJsonldArbitre.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

