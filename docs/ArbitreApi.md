# swagger_client.ArbitreApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_arbitre_collection**](ArbitreApi.md#get_arbitre_collection) | **GET** /api/arbitres | Retrieves the collection of Arbitre resources.
[**get_arbitre_item**](ArbitreApi.md#get_arbitre_item) | **GET** /api/arbitres/{inNo} | Retrieves a Arbitre resource.

# **get_arbitre_collection**
> InlineResponse2001 get_arbitre_collection(page=page)

Retrieves the collection of Arbitre resources.

Retrieves the collection of Arbitre resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ArbitreApi()
page = 1  # int | The collection page number (optional) (default to 1)

try:
    # Retrieves the collection of Arbitre resources.
    api_response = api_instance.get_arbitre_collection(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArbitreApi->get_arbitre_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitre_item**
> ArbitreJsonldArbitre get_arbitre_item(in_no)

Retrieves a Arbitre resource.

Retrieves a Arbitre resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.ArbitreApi()
in_no = 'in_no_example'  # str | Resource identifier

try:
    # Retrieves a Arbitre resource.
    api_response = api_instance.get_arbitre_item(in_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArbitreApi->get_arbitre_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **in_no** | **str**| Resource identifier | 

### Return type

[**ArbitreJsonldArbitre**](ArbitreJsonldArbitre.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

