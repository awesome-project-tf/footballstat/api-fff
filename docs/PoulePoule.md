# PoulePoule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stage_number** | **int** |  | [optional] 
**season** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**code** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**gp_diff_info** | **str** |  | [optional] 
**gp_diff_no_tour** | **int** |  | [optional] 
**phase** | [**PhasePoule**](PhasePoule.md) |  | [optional] 
**competition** | [**CompetPoule**](CompetPoule.md) |  | [optional] 
**cdg** | [**CdgPoule**](CdgPoule.md) |  | [optional] 
**rounds** | [**list[PouleJourneePoule]**](PouleJourneePoule.md) |  | [optional] 
**poule_unique** | **bool** |  | [optional] 
**at_least_one_match_resultat** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

