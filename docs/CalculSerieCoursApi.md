# swagger_client.CalculSerieCoursApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_calcul_serie_cours_collection**](CalculSerieCoursApi.md#get_calcul_serie_cours_collection) | **GET** /api/calcul_serie_cours | Retrieves the collection of CalculSerieCours resources.
[**get_calcul_serie_cours_item**](CalculSerieCoursApi.md#get_calcul_serie_cours_item) | **GET** /api/calcul_serie_cours/{id} | Retrieves a CalculSerieCours resource.

# **get_calcul_serie_cours_collection**
> InlineResponse2004 get_calcul_serie_cours_collection(page=page, sa_no=sa_no, cl_no=cl_no, eq_no=eq_no, cp_no=cp_no, exists_competition=exists_competition)

Retrieves the collection of CalculSerieCours resources.

Retrieves the collection of CalculSerieCours resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculSerieCoursApi()
page = 1  # int | The collection page number (optional) (default to 1)
sa_no = [56]  # list[int] |  (optional)
cl_no = [56]  # list[int] |  (optional)
eq_no = [56]  # list[int] |  (optional)
cp_no = [56]  # list[int] |  (optional)
exists_competition = true  # bool |  (optional)

try:
    # Retrieves the collection of CalculSerieCours resources.
    api_response = api_instance.get_calcul_serie_cours_collection(page=page, sa_no=sa_no, cl_no=cl_no, eq_no=eq_no,
                                                                  cp_no=cp_no, exists_competition=exists_competition)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculSerieCoursApi->get_calcul_serie_cours_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **sa_no** | [**list[int]**](int.md)|  | [optional] 
 **cl_no** | [**list[int]**](int.md)|  | [optional] 
 **eq_no** | [**list[int]**](int.md)|  | [optional] 
 **cp_no** | [**list[int]**](int.md)|  | [optional] 
 **exists_competition** | **bool**|  | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_calcul_serie_cours_item**
> CalculSerieCoursJsonldCalculSerieCours get_calcul_serie_cours_item(id)

Retrieves a CalculSerieCours resource.

Retrieves a CalculSerieCours resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.CalculSerieCoursApi()
id = 'id_example'  # str | Resource identifier

try:
    # Retrieves a CalculSerieCours resource.
    api_response = api_instance.get_calcul_serie_cours_item(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CalculSerieCoursApi->get_calcul_serie_cours_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Resource identifier | 

### Return type

[**CalculSerieCoursJsonldCalculSerieCours**](CalculSerieCoursJsonldCalculSerieCours.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

