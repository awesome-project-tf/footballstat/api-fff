# AdresseClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ai_no** | **int** |  | [optional] 
**py_cod** | **str** |  | [optional] 
**py_libelle** | **str** |  | [optional] 
**ai_dat_maj** | **datetime** |  | [optional] 
**ai_uti_maj** | **str** |  | [optional] 
**ai_statut** | **str** |  | [optional] 
**ai_dat** | **datetime** |  | [optional] 
**ai_typ** | **str** |  | [optional] 
**ai_typ_libelle** | **str** |  | [optional] 
**ai_adr1** | **str** |  | [optional] 
**ai_adr2** | **str** |  | [optional] 
**ai_adr3** | **str** |  | [optional] 
**ai_cp** | **int** |  | [optional] 
**ai_bdis** | **str** |  | [optional] 
**ai_insee** | **int** |  | [optional] 
**ai_geo** | **int** |  | [optional] 
**ai_perso** | **bool** |  | [optional] 
**ai_club** | **bool** |  | [optional] 
**ai_des** | **bool** |  | [optional] 
**ai_prof** | **bool** |  | [optional] 
**ai_poi** | **str** |  | [optional] 
**poi_latitude** | **str** |  | [optional] 
**poi_longitude** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

