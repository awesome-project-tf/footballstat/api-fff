# PhasePhase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**has_ranking** | **str** |  | [optional] 
**has_second_leg** | **str** |  | [optional] 
**has_penalty** | **str** |  | [optional] 
**has_extra_time** | **str** |  | [optional] 
**competition** | [**CompetPhase**](CompetPhase.md) |  | [optional] 
**groups** | [**list[PoulePhase]**](PoulePhase.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

