# ClubEngagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cl_no** | **int** |  | [optional] 
**affiliation_number** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

