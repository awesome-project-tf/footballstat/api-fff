# ClubEquipe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cl_no** | **int** |  | [optional] 
**district** | [**CdgEquipe**](CdgEquipe.md) |  | [optional] 
**affiliation_number** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**short_name** | **str** |  | [optional] 
**logo** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

