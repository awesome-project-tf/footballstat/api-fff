# IndividuJsonldClub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **OneOfIndividuJsonldClubContext** |  | [optional] 
**id** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**in_no** | **str** |  | [optional] 
**adresses** | [**list[AdresseJsonldClub]**](AdresseJsonldClub.md) |  | [optional] 
**contacts** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

