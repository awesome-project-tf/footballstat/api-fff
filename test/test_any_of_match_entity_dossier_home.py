# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import apifff
from apifff.models.any_of_match_entity_dossier_home import AnyOfMatchEntityDossierHome  # noqa: E501
from apifff.rest import ApiException


class TestAnyOfMatchEntityDossierHome(unittest.TestCase):
    """AnyOfMatchEntityDossierHome unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAnyOfMatchEntityDossierHome(self):
        """Test AnyOfMatchEntityDossierHome"""
        # FIXME: construct object with mandatory attributes with example values
        # model = apifff.models.any_of_match_entity_dossier_home.AnyOfMatchEntityDossierHome()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
