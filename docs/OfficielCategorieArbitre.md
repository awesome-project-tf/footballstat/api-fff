# OfficielCategorieArbitre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sa_no** | **int** |  | [optional] 
**ca_cod** | **str** |  | [optional] 
**ca_lib** | **str** |  | [optional] 
**ca_niv** | **str** |  | [optional] 
**ca_tri** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

