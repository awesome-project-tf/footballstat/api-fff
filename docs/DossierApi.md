# swagger_client.DossierApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_dossier_collection**](DossierApi.md#get_dossier_collection) | **GET** /api/dossiers | Retrieves the collection of Dossier resources.
[**get_dossier_item**](DossierApi.md#get_dossier_item) | **GET** /api/dossiers/{dsNo} | Retrieves a Dossier resource.

# **get_dossier_collection**
> InlineResponse20015 get_dossier_collection(page=page, in_no=in_no, in_no=in_no, td_cod=td_cod, td_cod=td_cod)

Retrieves the collection of Dossier resources.

Retrieves the collection of Dossier resources.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.DossierApi()
page = 1  # int | The collection page number (optional) (default to 1)
in_no = 56  # int |  (optional)
in_no = [56]  # list[int] |  (optional)
td_cod = 'td_cod_example'  # str |  (optional)
td_cod = ['td_cod_example']  # list[str] |  (optional)

try:
    # Retrieves the collection of Dossier resources.
    api_response = api_instance.get_dossier_collection(page=page, in_no=in_no, in_no=in_no, td_cod=td_cod,
                                                       td_cod=td_cod)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DossierApi->get_dossier_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional] [default to 1]
 **in_no** | **int**|  | [optional] 
 **in_no** | [**list[int]**](int.md)|  | [optional] 
 **td_cod** | **str**|  | [optional] 
 **td_cod** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**InlineResponse20015**](InlineResponse20015.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dossier_item**
> DossierJsonldDossier get_dossier_item(ds_no)

Retrieves a Dossier resource.

Retrieves a Dossier resource.

### Example

```python
from __future__ import print_function
import time
import apifff
from apifff.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = apifff.DossierApi()
ds_no = 'ds_no_example'  # str | Resource identifier

try:
    # Retrieves a Dossier resource.
    api_response = api_instance.get_dossier_item(ds_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DossierApi->get_dossier_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ds_no** | **str**| Resource identifier | 

### Return type

[**DossierJsonldDossier**](DossierJsonldDossier.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

