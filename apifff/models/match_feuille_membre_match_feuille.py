# coding: utf-8

"""

    # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 0.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class MatchFeuilleMembreMatchFeuille(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'mmf_no': 'int',
        'mm_no': 'int',
        'po_cod': 'str',
        'po_lib': 'str',
        'mmf_typ': 'str',
        'mmf_statut': 'str',
        'mmf_pres': 'str',
        'mmf_in_nom': 'str',
        'mmf_in_prenom': 'str',
        'mmf_typ_piece': 'str',
        'mmf_in_no': 'str'
    }

    attribute_map = {
        'mmf_no': 'mmf_no',
        'mm_no': 'mm_no',
        'po_cod': 'po_cod',
        'po_lib': 'po_lib',
        'mmf_typ': 'mmf_typ',
        'mmf_statut': 'mmf_statut',
        'mmf_pres': 'mmf_pres',
        'mmf_in_nom': 'mmf_in_nom',
        'mmf_in_prenom': 'mmf_in_prenom',
        'mmf_typ_piece': 'mmf_typ_piece',
        'mmf_in_no': 'mmf_in_no'
    }

    def __init__(self, mmf_no=None, mm_no=None, po_cod=None, po_lib=None, mmf_typ=None, mmf_statut=None, mmf_pres=None, mmf_in_nom=None, mmf_in_prenom=None, mmf_typ_piece=None, mmf_in_no=None):  # noqa: E501
        """MatchFeuilleMembreMatchFeuille - a model defined in Swagger"""  # noqa: E501
        self._mmf_no = None
        self._mm_no = None
        self._po_cod = None
        self._po_lib = None
        self._mmf_typ = None
        self._mmf_statut = None
        self._mmf_pres = None
        self._mmf_in_nom = None
        self._mmf_in_prenom = None
        self._mmf_typ_piece = None
        self._mmf_in_no = None
        self.discriminator = None
        if mmf_no is not None:
            self.mmf_no = mmf_no
        if mm_no is not None:
            self.mm_no = mm_no
        if po_cod is not None:
            self.po_cod = po_cod
        if po_lib is not None:
            self.po_lib = po_lib
        if mmf_typ is not None:
            self.mmf_typ = mmf_typ
        if mmf_statut is not None:
            self.mmf_statut = mmf_statut
        if mmf_pres is not None:
            self.mmf_pres = mmf_pres
        if mmf_in_nom is not None:
            self.mmf_in_nom = mmf_in_nom
        if mmf_in_prenom is not None:
            self.mmf_in_prenom = mmf_in_prenom
        if mmf_typ_piece is not None:
            self.mmf_typ_piece = mmf_typ_piece
        if mmf_in_no is not None:
            self.mmf_in_no = mmf_in_no

    @property
    def mmf_no(self):
        """Gets the mmf_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: int
        """
        return self._mmf_no

    @mmf_no.setter
    def mmf_no(self, mmf_no):
        """Sets the mmf_no of this MatchFeuilleMembreMatchFeuille.


        :param mmf_no: The mmf_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: int
        """

        self._mmf_no = mmf_no

    @property
    def mm_no(self):
        """Gets the mm_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mm_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: int
        """
        return self._mm_no

    @mm_no.setter
    def mm_no(self, mm_no):
        """Sets the mm_no of this MatchFeuilleMembreMatchFeuille.


        :param mm_no: The mm_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: int
        """

        self._mm_no = mm_no

    @property
    def po_cod(self):
        """Gets the po_cod of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The po_cod of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._po_cod

    @po_cod.setter
    def po_cod(self, po_cod):
        """Sets the po_cod of this MatchFeuilleMembreMatchFeuille.


        :param po_cod: The po_cod of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._po_cod = po_cod

    @property
    def po_lib(self):
        """Gets the po_lib of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The po_lib of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._po_lib

    @po_lib.setter
    def po_lib(self, po_lib):
        """Sets the po_lib of this MatchFeuilleMembreMatchFeuille.


        :param po_lib: The po_lib of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._po_lib = po_lib

    @property
    def mmf_typ(self):
        """Gets the mmf_typ of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_typ of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_typ

    @mmf_typ.setter
    def mmf_typ(self, mmf_typ):
        """Sets the mmf_typ of this MatchFeuilleMembreMatchFeuille.


        :param mmf_typ: The mmf_typ of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_typ = mmf_typ

    @property
    def mmf_statut(self):
        """Gets the mmf_statut of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_statut of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_statut

    @mmf_statut.setter
    def mmf_statut(self, mmf_statut):
        """Sets the mmf_statut of this MatchFeuilleMembreMatchFeuille.


        :param mmf_statut: The mmf_statut of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_statut = mmf_statut

    @property
    def mmf_pres(self):
        """Gets the mmf_pres of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_pres of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_pres

    @mmf_pres.setter
    def mmf_pres(self, mmf_pres):
        """Sets the mmf_pres of this MatchFeuilleMembreMatchFeuille.


        :param mmf_pres: The mmf_pres of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_pres = mmf_pres

    @property
    def mmf_in_nom(self):
        """Gets the mmf_in_nom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_in_nom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_in_nom

    @mmf_in_nom.setter
    def mmf_in_nom(self, mmf_in_nom):
        """Sets the mmf_in_nom of this MatchFeuilleMembreMatchFeuille.


        :param mmf_in_nom: The mmf_in_nom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_in_nom = mmf_in_nom

    @property
    def mmf_in_prenom(self):
        """Gets the mmf_in_prenom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_in_prenom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_in_prenom

    @mmf_in_prenom.setter
    def mmf_in_prenom(self, mmf_in_prenom):
        """Sets the mmf_in_prenom of this MatchFeuilleMembreMatchFeuille.


        :param mmf_in_prenom: The mmf_in_prenom of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_in_prenom = mmf_in_prenom

    @property
    def mmf_typ_piece(self):
        """Gets the mmf_typ_piece of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_typ_piece of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_typ_piece

    @mmf_typ_piece.setter
    def mmf_typ_piece(self, mmf_typ_piece):
        """Sets the mmf_typ_piece of this MatchFeuilleMembreMatchFeuille.


        :param mmf_typ_piece: The mmf_typ_piece of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_typ_piece = mmf_typ_piece

    @property
    def mmf_in_no(self):
        """Gets the mmf_in_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501


        :return: The mmf_in_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :rtype: str
        """
        return self._mmf_in_no

    @mmf_in_no.setter
    def mmf_in_no(self, mmf_in_no):
        """Sets the mmf_in_no of this MatchFeuilleMembreMatchFeuille.


        :param mmf_in_no: The mmf_in_no of this MatchFeuilleMembreMatchFeuille.  # noqa: E501
        :type: str
        """

        self._mmf_in_no = mmf_in_no

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(MatchFeuilleMembreMatchFeuille, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MatchFeuilleMembreMatchFeuille):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
